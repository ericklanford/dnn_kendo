﻿using Dnn_Kendo_Generator;
using Dnn_Kendo_Generator.JavaScriptElements;
using Dnn_Kendo_Generator.Kendo_Datasource;
using Dnn_Kendo_Generator.Kendo_Grid;
using Dnn_Kendo_Generator.Kendo_References;
using Dnn_Kendo_Generator.KendoGridColumn;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Util;

namespace Generator
{
    public static class DnnGenerateProject
    {
        public static string generate_Project(string projectName)
        {
            const string sourcePath = @"C:\Dnn_kendo\Dnn_Kendo_Generator\Base_Dnn_Project\DNN_TEMP_PROJECT_NAME\";
            var destinationPath = @"C:\Dnn_Kendo_Projects\" + projectName + @"\";

            //Now Create all of the directories
            foreach (var dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (var newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                var currentFilePath = newPath.Replace(sourcePath, destinationPath);

                File.Copy(newPath, currentFilePath, true);

                if (currentFilePath.EndsWith(".gif") || currentFilePath.Contains("\\packages\\"))
                {
                    continue;
                }

                var text = File.ReadAllText(currentFilePath);
                text = text.Replace("DNN_TEMP_PROJECT_NAME", projectName);
                File.WriteAllText(currentFilePath, text);
            }

            //replace names
            File.Move(destinationPath + "DNN_TEMP_PROJECT_NAME.dnn", destinationPath + projectName + ".dnn");
            File.Move(destinationPath + "DNN_TEMP_PROJECT_NAME.csproj.user", destinationPath + projectName + ".csproj.user");
            File.Move(destinationPath + "DNN_TEMP_PROJECT_NAME.csproj", destinationPath + projectName + ".csproj");
            File.Move(destinationPath + "DNN_TEMP_PROJECT_NAMEModuleBase.cs", destinationPath + projectName + "ModuleBase.cs");
            File.Move(destinationPath + "DNN_TEMP_PROJECT_NAMEModuleSettingsBase.cs", destinationPath + projectName + "ModuleSettingsBase.cs");
            File.Move(destinationPath + "DNN_TEMP_PROJECT_NAME.sln", destinationPath + projectName + ".sln");

            return destinationPath + projectName + ".csproj";
        }

        public static void generate_Project(string projectName, KendoReferences references, KendoGrid kendoComponent)
        {
            generate_Project(projectName);

            File.AppendAllText(@"C:\Dnn_Kendo_Projects\" + projectName + @"\View.ascx",

                $@"{references.GetSource()}
                   {kendoComponent.GetHtmlPart()}
                    <script>
                        $(function() {{
                            {kendoComponent.GetSource()}
                        }});
                    </script>"
                );
        }

        public static void generate_Project(string projectName, KendoReferences references, List<KendoComponent> listOfComponents)
        {
            generate_Project(projectName);

            File.AppendAllText(@"C:\Dnn_Kendo_Projects\" + projectName + @"\View.ascx",

                $@"{references.GetSource()}
                   {string.Concat(listOfComponents.Select(m => m.GetHtmlPart()))}
                    <script type=""text/javascript"">
                        $(function() {{
                            {string.Concat(listOfComponents.Select(m => m.GetSource()))}
                        }});
                    </script>"
                );
        }

        public static string generate_Project(string projectName, AlwaysUpdateFromJsonCall alwaysUpdateFromJsonCall)
        {
            var projectPath = $@"C:\Dnn_Kendo_Projects\{projectName}";
            generate_Project(projectName);
            File.AppendAllText(projectPath + @"\View.ascx", JavaScriptHelper.GetCode(alwaysUpdateFromJsonCall));

            return projectPath + @"\" + projectName + ".csproj";
        }

        public static void Generate_Modules_From_Json(string projectName, string readUrl, string jsonObject)
        {
            generate_Project(projectName, new KendoReferences(), Utils.GetComponentsFromJsonString(readUrl, $"comp_{projectName}", jsonObject));
        }

        public static string Generate_From_Url_Json_Always_Updated(string projectName, string readUrl, string user = "", string password = "")
        {
            string response;

            using (var client = user == "" ? new WebClient() : new WebClient { Credentials = new NetworkCredential(user, password) })
            {
                response = client.DownloadString(readUrl);
            }

            return Generate_Modules_From_Json_Always_Updated(projectName, readUrl, response);
        }

        public static string Generate_Modules_From_Json_Always_Updated(string projectName, string readUrl, string jsonObject)
        {
            return generate_Project(projectName, Utils.GetComponentsFromJson_Always_Updated(readUrl, $"comp_{projectName}", jsonObject));
        }

        public static void Generate_Modules_From_Html_Table(string projectName, string htmlTable)
        {
            generate_Project(projectName, new KendoReferences(), Utils.GetComponentsFromHtmlTableString($"comp_{projectName}", htmlTable));
        }

        public static void Generate_Modules_From_Url_Json(string prefix, string readUrl, string user = "", string password = "")
        {
            string response;

            using (var client = user == "" ? new WebClient() : new WebClient { Credentials = new NetworkCredential(user, password) })
            {
                response = client.DownloadString(readUrl);
            }

            var projectName = prefix + readUrl.Split('/').Last();

            //check if readUrl have parameters (?)
            if (projectName.Contains("?"))
            {
                projectName = projectName.Substring(0, projectName.IndexOf('?'));
            }

            Generate_Modules_From_Json(projectName, readUrl, response);
        }

        private static void Generate_Projects(string projectName, string readUrl, List<KendoGridColumn> columns)
        {
            //simple module
            generate_simple_module(projectName + "_DNN_Simple_module", columns, readUrl);

            //crud operations
            generate_crud_module(projectName + "_DNN_CRUD_module", new KendoDatasourceCrud(readUrl, "updateUrl", "destroyUrl", "createUrl", columns));

            //always online
            generate_always_online_module(projectName + "_DNN_AlwaysOnline_module", readUrl, columns);
        }

        private static void generate_simple_module(string projectName, List<KendoGridColumn> columns, string readUrl)
        {
            generate_Project(projectName, new KendoReferences(), new KendoGrid(readUrl, $"comp_{projectName}", columns));
        }

        private static void generate_crud_module(string projectName, KendoDatasourceCrud kendoDatasourceCrud)
        {
            generate_Project(projectName, new KendoReferences(), new KendoGridCrud($"comp_{projectName}", kendoDatasourceCrud));
        }

        private static void generate_always_online_module(string projectName, string readUrl, List<KendoGridColumn> columns)
        {
            generate_Project(projectName, Utils.GetComponentsFromJson_Always_Updated(readUrl, $"comp_{projectName}", columns));
        }

        private static void generate_DataService(string dataServiceName)
        {//FIX
            var sourcePath = @"..\..\..\Dnn_Kendo_Generator\Base_Dnn_Project\DemoDataService\";
            var destinationPath = @"C:\Dnn_Kendo_Projects\" + dataServiceName + @"\";

            //Now Create all of the directories
            foreach (var dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (var newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                var currentFilePath = newPath.Replace(sourcePath, destinationPath);

                File.Copy(newPath, currentFilePath, true);

                var text = File.ReadAllText(currentFilePath);
                text = text.Replace("DemoDataService", dataServiceName);
                File.WriteAllText(currentFilePath, text);
            }

            //replace names
            File.Move(destinationPath + "DemoDataService.dnn", destinationPath + dataServiceName + ".dnn");
            File.Move(destinationPath + "DemoDataService.csproj.user", destinationPath + dataServiceName + ".csproj.user");
            File.Move(destinationPath + "DemoDataService.csproj", destinationPath + dataServiceName + ".csproj");
            File.Move(destinationPath + "DemoDataServiceModuleBase.cs", destinationPath + dataServiceName + "ModuleBase.cs");
            File.Move(destinationPath + "DemoDataServiceModuleSettingsBase.cs", destinationPath + dataServiceName + "ModuleSettingsBase.cs");
            File.Move(destinationPath + "DemoDataService.sln", destinationPath + dataServiceName + ".sln");
        }

        #region DB
        public static void generate_Modules_From_Db(string connectionString, string readUrl)
        {
            var dbName = Utils.get_Db_Name(connectionString);
            var dbTables = Utils.get_Db_Tables(connectionString);

            generate_DataService(dbName + "_DataService");

            foreach (var table in dbTables)
            {
                var columns = Utils.get_Db_Columns(connectionString, dbName, table);
                Generate_Projects(table, readUrl, columns);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dnn_Kendo_Generator
{
    public class KendoMultiple : KendoComponent
    {
        private List<KendoComponent> _components = new List<KendoComponent>();

        public void AddComponet(KendoComponent kendoComponent)
        {
            _components.Add(kendoComponent);
        }


        public override string GetHtmlPart()
        {
            return string.Join(Environment.NewLine, _components.Select(m => m.GetHtmlPart()));
        }

        public override string GetSource()
        {
            return string.Join(Environment.NewLine, _components.Select(m => m.GetSource()));
        }

        public override string GetUpdateSource()
        {
            return string.Join(Environment.NewLine, _components.Select(m => m.GetUpdateSource()));
        }
    }
}

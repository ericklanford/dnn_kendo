﻿namespace Dnn_Kendo_Generator.KendoGridColumn
{
    public class KendoGridViewColumnCommand
    {
        public string Name { get; set; }

        public string ClassName { get; set; }

        public string Click { get; set; }

        public string Text { get; set; }


        internal string SerialiceAsJson()
        {
            var result = @"{
        ""name"": """ + Name + @""",
        ""className"": """ + ClassName + @""",
        ""click"": " + Click + @",
        ""text"": """ + Text + @"""
}";
            return result;
        }
    }
}
﻿using System.Linq;

namespace Dnn_Kendo_Generator.KendoGridColumn
{
    public class KendoGridColumn
    {
        public KendoGridColumn(string field) : this(field, field)
        {
        }
        public KendoGridColumn(string field, string title)
        {
            Field = field;
            Title = title;
        }
        public KendoGridColumn(string field, string title, string dataType, int characterMaximumLength) : this(field, title)
        {
            DataType = dataType;
            CharacterMaximumLength = characterMaximumLength;
        }

        public string DataType { get; set; }

        public int CharacterMaximumLength { get; set; }

        public string Field { get; set; }

        public string Title { get; set; }

        public bool Hidden { get; set; }

        public string Encoded { get; set; }

        public string Template { get; set; }

        public string Width { get; set; }

        public string Filterable { get; set; }

        public KendoGridViewColumnCommand[] Command { get; set; }


        internal string SerialiceAsJson()
        {
            var result = @"
            {
                field: '" + Field + @"',
                title: '" + Title + @"'" +
                (Hidden ? @",hidden: true" : "") +
                (string.IsNullOrEmpty(Encoded) ? "" : @",
                encoded: " + Encoded) +
                (string.IsNullOrEmpty(Template) ? "" : @",
                template: '" + Template + @"'") +
                (string.IsNullOrEmpty(Filterable) ? "" : @",
                filterable: " + Filterable) +
                (string.IsNullOrEmpty(Width) ? "" : @",
                width: '" + Width + @"'") +
                (Command == null ? "" : @",
                command: [" + string.Join(",", Command.Select(m => m.SerialiceAsJson())) + @"
                ]") +
            "}";
            return result;
        }
    }
}

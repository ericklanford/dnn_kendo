﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dnn_Kendo_Generator
{
    public class KendoTemplate : KendoComponent
    {
        private string _id;
        string _dataJsonObjName;
        private Dictionary<string, string> _properties;

        public KendoTemplate(string id, string dataJsonObjName, Dictionary<string, string> properties)
        {
            _id = id;
            _dataJsonObjName = dataJsonObjName;
            _properties = properties;
        }

        public KendoTemplate(string dataJsonObjName)
        {
            _id = Guid.NewGuid().ToString("N");
            _dataJsonObjName = dataJsonObjName;
            _properties = new Dictionary<string, string>();
        }

        public void AddProperty(string propertyName)
        {
            _properties.Add(propertyName, propertyName);
        }

        public override string GetHtmlPart()
        {
            return
                $@"
                <div id=""kendoTemplate_Html_{_id}""></div>
                <script type=""text/x-kendo-template"" id=""kendoTemplate_Definition_{_id}"">   
                    {string.Join("\n", from m in _properties.Keys select $"<label>{m}</label> # data.{_properties[m]} #")}
                </script>";
        }

        public override string GetSource()
        {
            return
                $@"
                    $('#kendoTemplate_Html_{_id}').html(kendo.template($(""#kendoTemplate_Definition_{_id}"").html())({_dataJsonObjName}));
                ";
        }

        public override string GetUpdateSource()
        {
            return
                $@"
                    $('#kendoTemplate_Html_{_id}').html(kendo.template($(""#kendoTemplate_Definition_{_id}"").html())({_dataJsonObjName}));
                ";
        }
    }
}

﻿using Dnn_Kendo_Generator.Kendo_Datasource;
using System.Linq;

namespace Dnn_Kendo_Generator.Kendo_Grid
{
    public class KendoGridCrud : KendoGrid
    {
        public KendoGridCrud(string _HtmlId, KendoDatasourceCrud kendoDatasourceCrud) : base(_HtmlId, kendoDatasourceCrud)
        {
        }
        public override string GetSource()
        {
            return $@"$('#{HtmlId}').kendoGrid({{
                dataSource:{Datasource.GetSource()},
                toolbar: ['create'],
                columns:[{string.Join(",", Datasource.Columns.Select(m => m.SerialiceAsJson()))},
                    {{ command: ['edit', 'destroy'], title: ' & nbsp; ' }}
                ]
            }});";
        }
    }
}

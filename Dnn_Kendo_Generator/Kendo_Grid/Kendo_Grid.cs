﻿using Dnn_Kendo_Generator.Kendo_Datasource;
using System.Collections.Generic;
using System.Linq;

namespace Dnn_Kendo_Generator.Kendo_Grid
{
    public class KendoGrid : KendoComponent
    {
        public string HtmlId { get; set; }
        public KendoDatasource Datasource { get; set; }

        public KendoGrid(string htmlId)
        {
            HtmlId = htmlId;
            Datasource = new KendoDatasource();
        }

        public KendoGrid(string htmlId, KendoDatasource datasource)
        {
            HtmlId = htmlId;
            Datasource = datasource;
        }

        public KendoGrid(string readUrl, string _HtmlId, List<KendoGridColumn.KendoGridColumn> columns) : this(_HtmlId)
        {
            Datasource = new KendoDatasourceReadOnly(readUrl, columns);
        }

        public override string GetSource()
        {
            return $@"$('#{HtmlId}').kendoGrid({{
                dataSource:{Datasource.GetSource()},
                columns:[{string.Join(",", Datasource.Columns.Select(m => m.SerialiceAsJson()))}
                ]
            }});";
        }

        public override string GetHtmlPart()
        {
            return $@"<div id=""{HtmlId}""></div>";
        }

        public override string GetUpdateSource()
        {
            return $@"$(""#{HtmlId}"").data(""kendoGrid"").dataSource.data({Datasource.GetUpdateSource()});";
        }
    }
}

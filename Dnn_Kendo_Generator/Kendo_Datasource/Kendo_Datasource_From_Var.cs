﻿using System.Collections.Generic;

namespace Dnn_Kendo_Generator.Kendo_Datasource
{
    public class KendoDatasourceFromVar : KendoDatasource
    {
        string _varName;

        public KendoDatasourceFromVar(List<KendoGridColumn.KendoGridColumn> columns, string varName) : base(columns, $@"{{data: {varName}}}")
        {
            _varName = varName;
        }

        public override string GetUpdateSource()
        {
            return _varName;
        }
    }
}

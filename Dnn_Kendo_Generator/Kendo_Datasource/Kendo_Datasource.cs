﻿using System.Collections.Generic;
using System.Xml;

namespace Dnn_Kendo_Generator.Kendo_Datasource
{
    public class KendoDatasource : KendoComponent
    {
        internal List<KendoGridColumn.KendoGridColumn> Columns;
        private readonly string _sourceValues;

        public KendoDatasource()
        {
            Columns = new List<KendoGridColumn.KendoGridColumn>() { new KendoGridColumn.KendoGridColumn("Column1"), new KendoGridColumn.KendoGridColumn("Column2") };
            _sourceValues = @"[ {Column1:10,Column2:10}, {Column1:20,Column2:20}, {Column1:30,Column2:30} ]";
        }

        public KendoDatasource(List<KendoGridColumn.KendoGridColumn> columns, string sourceValues)
        {
            Columns = columns;
            _sourceValues = sourceValues;
        }

        public KendoDatasource(XmlDocument doc)
        {
            Columns = new List<KendoGridColumn.KendoGridColumn>();

            foreach (XmlNode item in doc.GetElementsByTagName("th"))
            {
                Columns.Add(new KendoGridColumn.KendoGridColumn(item.ChildNodes[0].Value.Replace(" ", "_"), item.ChildNodes[0].Value));
            }
            _sourceValues = "";

            foreach (XmlNode item in doc.GetElementsByTagName("th"))
            {
                Columns.Add(new KendoGridColumn.KendoGridColumn(item.ChildNodes[0].Value));
            }
        }

        public override string GetSource()
        {
            return _sourceValues;
        }

        public override string GetHtmlPart()
        {
            return string.Empty;
        }

        public override string GetUpdateSource()
        {
            return string.Empty;
        }
    }
}

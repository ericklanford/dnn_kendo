﻿using System.Collections.Generic;

namespace Dnn_Kendo_Generator.Kendo_Datasource
{
    public class KendoDatasourceReadOnly : KendoDatasource
    {
        public string ReadUrl { get; set; }

        public KendoDatasourceReadOnly(string readUrl, List<KendoGridColumn.KendoGridColumn> columns)
        {
            ReadUrl = readUrl;
            Columns = columns;
        }

        public override string GetSource()
        {
            //return $@"new kendo.data.DataSource({{ batch: true, transport: {{ read:  {{ url: '{ReadUrl}', dataType: 'jsonp' }} }}, schema: {{ model: {{ id: '{columns[0].field}' }} }} }})";

            return $@"
            {{
                transport:
                {{
                    read:
                    {{
                        url: '{ReadUrl}',
                        dataType: 'json',
                        type: 'GET'
                    }}
                }}
            }}";
        }
    }
}

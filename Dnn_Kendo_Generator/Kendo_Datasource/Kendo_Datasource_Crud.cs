﻿using System.Collections.Generic;

namespace Dnn_Kendo_Generator.Kendo_Datasource
{
    public class KendoDatasourceCrud : KendoDatasourceReadOnly
    {
        public string UpdateUrl { get; set; }
        public string DestroyUrl { get; set; }
        public string CreateUrl { get; set; }

        public KendoDatasourceCrud(string readUrl, string updateUrl, string destroyUrl, string createUrl, List<KendoGridColumn.KendoGridColumn> columns) : base(readUrl, columns)
        {
            UpdateUrl = updateUrl;
            DestroyUrl = destroyUrl;
            CreateUrl = createUrl;
        }

        public override string GetSource()
        {
            return $@"new kendo.data.DataSource({{
                            transport: {{
                                read:  {{
                                    url: '{ReadUrl}',
                                    dataType: 'jsonp'
                                }},
                                update: {{
                                    url: '{UpdateUrl}',
                                    dataType: 'jsonp'
                                }},
                                destroy: {{
                                    url: '{DestroyUrl}',
                                    dataType: 'jsonp'
                                }},
                                create: {{
                                    url: '{CreateUrl}',
                                    dataType: 'jsonp'
                                }},
                                parameterMap: function(options, operation)
{{
    if (operation !== 'read' && options.models)
    {{
        return {{ models: kendo.stringify(options.models)}};
    }}
}}
                            }},
                            batch: true,
                            schema: {{
                                model: {{
                                    id: 'Id'
                                }}
                            }}
                        }});";
        }
    }
}

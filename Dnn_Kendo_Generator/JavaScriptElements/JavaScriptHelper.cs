﻿using System;

namespace Dnn_Kendo_Generator.JavaScriptElements
{
    public static class JavaScriptHelper
    {
        public static string GetCode(KendoComponent kendoComponent)
        {
            return
                $@"{kendoComponent.GetHtmlPart()}
                    {Environment.NewLine}
                    <script type=""text/javascript"">                    
                            {kendoComponent.GetSource()}
                    </script>";
        }
    }
}

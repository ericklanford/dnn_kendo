﻿using System;

namespace Dnn_Kendo_Generator
{
    public class AlwaysUpdateFromJsonCall : KendoComponent
    {
        private static string _ajaxSuccessName = "dataAjax";
        TimeSpan _timeToRefresh;
        string _apiUrl;

        KendoComponent _componentToUpdate;

        public AlwaysUpdateFromJsonCall(string apiUrl, KendoComponent componentToUpdate, TimeSpan timeToRefresh)
        {
            _apiUrl = apiUrl.Replace("http://", "").Replace("https://", "");
            _apiUrl = _apiUrl.Substring(_apiUrl.IndexOf("/"));

            _componentToUpdate = componentToUpdate;
            _timeToRefresh = timeToRefresh;
        }

        public override string GetHtmlPart()
        {
            return
                "<span id=\"updatedInfo\"></span>"
                + _componentToUpdate.GetHtmlPart();
        }

        public override string GetSource()
        {
            return
                $@"
                    var refresh_JsonApiAjaxCall = null;
                    var timer_JsonApi = null;
                    var runningMode = 1; //fix this on Dnn_Kendo

                    $(function() {{
                        initGrid_JsonApi();
                    }});

                    function initGrid_JsonApi() {{
                        $.ajax({{
                            type: ""GET"",
                            url: ""{_apiUrl}"",
                            success: function ({_ajaxSuccessName}) {{
                                {_componentToUpdate.GetSource()}
                                $(""#updatedInfo"").html(""Last Updated: "" + moment().format(""MMM/Do/YYYY H:mm:ss A""));
                            }},
                            complete: function () {{
                                setTimeout(function () {{ refresh_JsonApi(); }}, {_timeToRefresh.TotalMilliseconds});
                            }},
                            error: function (xhr, ajaxOptions, thrownError) {{
                                if (xhr && xhr.status === 401) {{
                                    location.reload();
                                }}
                                    console.log(xhr);
                                    console.log(ajaxOptions);
                                    console.log(thrownError);
                            }}
                        }});
                    }}//initGrid_JsonApi

                    function refresh_JsonApi() {{

                        if (refresh_JsonApiAjaxCall) {{
                            refresh_JsonApiAjaxCall.abort();
                        }}

                        if (timer_JsonApi != null) {{
                            window.clearTimeout(timer_JsonApi);
                            timer_JsonApi = null;
                        }}

                        if (runningMode === 1 || (runningMode === 0 && userOnPage) || !$(""#updatedInfo"").html()) {{
                            refresh_JsonApiAjaxCall = $.ajax({{
                                type: ""GET"",
                                url: ""{_apiUrl}"",
                                // data: {{ hours: $(""#selDays"").val() * 24, direction: ""A"" }},
                                success: function ({_ajaxSuccessName}) {{
                                    {_componentToUpdate.GetUpdateSource()}
                                    $(""#updatedInfo"").html(""Last Updated: "" + moment().format(""MMM/Do/YYYY H:mm:ss A""));
                                }},
                                error: function (xhr, textStatus, errorThrown) {{
                                    console.log(xhr);
                                    console.log(textStatus);
                                    console.log(errorThrown);
                                }},
                                complete: function () {{
                                    refresh_JsonApiAjaxCall = null;
                                    timer_JsonApi = setTimeout(function () {{ refresh_JsonApi(); }}, 60000);
                                }}
                            }});
                        }}
                    }}
                ";
        }

        public override string GetUpdateSource()
        {
            return string.Empty;
        }
    }
}

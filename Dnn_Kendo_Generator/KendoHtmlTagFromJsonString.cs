﻿using System;

namespace Dnn_Kendo_Generator
{
    public class KendoHtmlTagFromJsonString : KendoComponent
    {
        private string _nameProperty;
        private string _valueProperty;

        public KendoHtmlTagFromJsonString(string name, string value)
        {
            _nameProperty = name;
            _valueProperty = value;
        }

        public override string GetHtmlPart()
        {
            return $"<div><label>{_nameProperty}</label>{_valueProperty}</div>";
        }

        public override string GetSource()
        {
            return string.Empty;
        }

        public override string GetUpdateSource()
        {
            throw new NotImplementedException();
        }
    }
}

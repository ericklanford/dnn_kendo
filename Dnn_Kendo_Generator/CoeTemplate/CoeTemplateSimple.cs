﻿using System;
using System.Linq;

namespace Dnn_Kendo_Generator.CoeTemplate
{
    public class CoeTemplateSimple : CoeTemplate
    {
        public CoeTemplateSimple(string nameProperty)
        {
            NameProperty = nameProperty;
            HtmlId = Guid.NewGuid().ToString("N");
        }

        public override string GetHtmlPart()
        {
            return $@"
                    <div id=""template_{HtmlId}"">
                        <label id=""label_{HtmlId}"">{NameProperty.Split('.').Last().Replace("_", " ")}</label>
                        <span id=""value_{HtmlId}""></span>
                    </div>";
        }

        public override string GetSource()
        {
            return $@"$('#value_{HtmlId}').html({NameProperty});";
        }

        public override string GetUpdateSource()
        {
            return $@"$('#value_{HtmlId}').html({NameProperty});";
        }
    }
}

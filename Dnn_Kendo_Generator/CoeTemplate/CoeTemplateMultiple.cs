﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dnn_Kendo_Generator.CoeTemplate
{
    public class CoeTemplateMultiple : CoeTemplate
    {

        public List<KendoComponent> Components { get; private set; }

        public CoeTemplateMultiple()
        {
            Components = new List<KendoComponent>();
        }

        public override string GetHtmlPart()
        {
            return string.Join(Environment.NewLine, Components.Select(m => m.GetHtmlPart()));
        }

        public override string GetSource()
        {
            return string.Join(Environment.NewLine, Components.Select(m => m.GetSource()));
        }

        public override string GetUpdateSource()
        {
            return string.Join(Environment.NewLine, Components.Select(m => m.GetUpdateSource()));
        }
    }
}

﻿using System;

namespace Dnn_Kendo_Generator
{
    public class KendoHtmlTag : KendoComponent
    {
        public KendoHtmlTag(string text)
        {
            Value = text;
        }

        private string Value { get; set; }

        public override string GetHtmlPart()
        {
            return $"<label>{Value}</label>";
        }

        public override string GetSource()
        {
            return string.Empty;
        }

        public override string GetUpdateSource()
        {
            throw new NotImplementedException();
        }
    }
}

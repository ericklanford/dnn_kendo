﻿namespace Dnn_Kendo_Generator
{
    public abstract class KendoComponent
    {
        public abstract string GetSource();
        public abstract string GetHtmlPart();
        public abstract string GetUpdateSource();
    }
}

﻿using Dnn_Kendo_Generator.Kendo_Grid;
using System.Linq;

namespace Dnn_Kendo_Generator
{
    public class KendoPropertiesGrids : KendoComponent
    {
        private readonly string _readUrl;
        private readonly string[] _properties;
        private readonly KendoGrid[] _grids;


        public KendoPropertiesGrids(string readUrl, string[] properties, KendoGrid[] grids)
        {
            _readUrl = readUrl;
            _properties = properties;
            _grids = grids;
        }

        private string GetGridsSources()
        {
            return string.Join("", _grids.Select(m => $@"
                if ($('#{m.HtmlId}').data('kendoGrid')) {{
                    $('#{m.HtmlId}').data('kendoGrid').dataSource.data(dataAjax.{m.HtmlId});
                }}
                else {{
                    {m.GetSource()}
                }}"));
        }

        private string GetPropertiesSources()
        {
            return string.Join("", _properties.Select(m => $"$('#{m}').html(dataAjax.{m});"));
        }

        public override string GetHtmlPart()
        {
            return string.Join("", _properties.Select(m => $"<label>{m.Replace('_', ' ')}: </label><span id = '{m}'></span>"))
            +
            string.Join("", _grids.Select(m => $"{m.GetHtmlPart()}"));
        }

        public override string GetSource()
        {
            return $@"
                    $(function(){{updateData();}});

                    var updateDataAjaxCall;
                    function updateData() {{
                        if (updateDataAjaxCall) {{
                            updateDataAjaxCall.abort();
                        }}
                        updateDataAjaxCall = $.ajax({{
                            type: 'GET',
                            url: '{_readUrl}',
                            dataType: 'json',
                            success: function (dataAjax) {{
                                {GetGridsSources()}                        
                                {GetPropertiesSources()}
                            }},
                            complete: function()
                            {{
                                updateDataAjaxCall = null;
                                setTimeout(function () {{ updateData(); }}, 60000);
                            }},
                            error: function(xhr)
                            {{
                                if (xhr && xhr.status == 401)
                                {{
                                    location.reload();
                                }}
                            }}
                        }});
                    }}";
        }

        public override string GetUpdateSource()
        {
            return $@"updateData();";
        }
    }
}

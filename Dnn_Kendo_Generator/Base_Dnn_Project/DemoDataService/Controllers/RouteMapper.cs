﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetNuke.Web.Api;

namespace Coesolutions.Modules.DemoDataService.Services
{
    public class RouterMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute(
                "DemoDataService",
                "default",
                "{controller}/{action}",
                new string[] { "Coesolutions.Modules.DemoDataService.Services" });
        }

    }
}

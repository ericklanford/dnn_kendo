﻿using Coesolutions.Modules.DemoDataService.Models;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Web.Api;
using Pervasive.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Coesolutions.Modules.DemoDataService.Services
{
    public class MainController : ControllerBase
    {
        #region "Web Methods"
        [DnnAuthorize()]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = new HttpResponseMessage();
            response.Content = new StringContent("<html><body>Hello World</body></html>");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        [AllowAnonymous]
        // [DnnAuthorize()]
        // POST api/savefile
        public HttpResponseMessage Post([FromBody]FileData file)
        {
            var data = Convert.FromBase64String(file.Base64);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(data))
            };

            result.Content.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);

            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = file.FileName
            };

            return result;
        }


        [DnnAuthorize()]
        [HttpGet()]
        public object TestResponseMdShipDetailsNotes(string vesselid = "")
        {
            try
            {
                #region TestResponse
                string testResponse = @"{
  ""LloydsID"": 9377470,
  ""CurrentShipName"": ""GRANDE SENEGAL"",
  ""ShipType"": ""Ro-Ro Cargo Ship"",
  ""Flag"": ""ITALY"",
  ""CallSign"": ""ICEU"",
  ""YearOfBuild"": 2010,
  ""GrossTons"": 47218,
  ""DeadweightTons"": 26653,
  ""NetTons"": 14165,
  ""Speed"": 21.8,
  ""MMSI"": ""247285500"",
  ""LengthIndicator"": ""OA"",
  ""BreadthIndicator"": ""M"",
  ""LengthMeters"": 210.92,
  ""LengthFeet"": 692,
  ""LengthInches"": 0,
  ""Breadth"": 32.26,
  ""BreadthFeet"": 105,
  ""BreadthInches"": 10,
  ""MoldedDepthMeters"": 13.34,
  ""MoldedDepthFeet"": 43,
  ""MoldedDepthInches"": 9,
  ""Draught"": 9.75,
  ""DraughtFeet"": 32,
  ""DraughtInches"": 0,
  ""Power"": 19040,
  ""NumberOfEngines"": 1,
  ""Propulsion"": ""Oil Engine(s), Direct Drive"",
  ""RPM"": 105,
  ""Notes"": [
    {
      ""NoteID"": 20727,
      ""LloydsID"": 9377470,
      ""UserID"": 0,
      ""Subject"": ""Orders:"",
      ""Note"": ""</br>Draft: 26'05\""</br>Speed: 18 knots</br>Wind: SW 10</br>Seas: 1'- 2'</br>Visibility: 8 Miles, scattered t-storms</br>Air temp.: 76 F</br>Tidal current at Bay ent.:</br>Slack   Max   Knots</br>1803    2129  1.7F"",
      ""DateCreated"": ""2016-08-19T16:09:05.11"",
      ""DateChanged"": ""2016-08-19T16:09:05.11"",
      ""AddedBy"": ""tbarco"",
      ""LastUpdatedBy"": """"
    }
  ],
  ""Propellers"": [
    {
      ""LloydsID"": 9377470,
      ""Sequence"": ""01"",
      ""PropellerPosition"": ""Centre Or Only"",
      ""PropellerType"": ""Propeller : Controllable Pitch"",
      ""PropellerTypeCode"": ""CP"",
      ""RPMService"": 0,
      ""RPMMaximum"": 105,
      ""NozzleType"": ""Nozzle : Unknown""
    }
  ],
  ""Thrusters"": [
    {
      ""LloydsID"": 9377470,
      ""Sequence"": ""01"",
      ""ThrusterType"": ""Tunnel thruster"",
      ""ThrusterTypeCode"": ""TU"",
      ""NumberOfThrusters"": 2,
      ""ThrusterPosition"": ""Forward"",
      ""ThrusterBHP"": 1360,
      ""ThrusterKW"": 1000,
      ""TypeOfInstallation"": ""Not Applicable""
    },
    {
      ""LloydsID"": 9377470,
      ""Sequence"": ""02"",
      ""ThrusterType"": ""Tunnel thruster"",
      ""ThrusterTypeCode"": ""TU"",
      ""NumberOfThrusters"": 1,
      ""ThrusterPosition"": ""Aft"",
      ""ThrusterBHP"": 1360,
      ""ThrusterKW"": 1000,
      ""TypeOfInstallation"": ""Not Applicable""
    }
  ]
}";
                #endregion

                var j = new JavaScriptSerializer();
                return j.Deserialize(testResponse, typeof(object));
            }
            catch (Exception ex)
            {
                //Log to DotNetNuke and reply with Error
                Exceptions.LogException(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [DnnAuthorize()]
        [HttpGet()]
        public VesselHistory[] TestGetArrayOfVesselHistory(string voyageid = "")
        {
            try
            {
                return new[]
                {
                    new VesselHistory
                    {
                        Draft = "29'0",
                        ElapsedTimeString = "N/A",
                        EndLoc = "Baltimore",
                        MovementStatus = "Tentative",
                        StartLoc = "Cape Henry",
                        TransitID = "85455",
                        VoyageId = "32770"
                    },
                    new VesselHistory
                    {
                        ElapsedTimeString = "N/A",
                        EndLoc = "Cape Henry",
                        MovementStatus = "Tentative",
                        StartLoc = "Baltimore",
                        TransitID = "85456",
                        VoyageId = "32770"
                    }
                };
            }
            catch (Exception ex)
            {
                //Log to DotNetNuke and reply with Error
                Exceptions.LogException(ex);
                return null;
            }
        }

        [DnnAuthorize()]
        [HttpGet]
        public List<Rundown_Inbound> RundownBase(string UsePilotCode = "")
        {
            return SqlHelper.SqlHelper.sqlRundownBase(conn(), UsePilotCode);
        }


        [DnnAuthorize()]
        [HttpGet]
        public List<RundownDatabounds> RundownBaseOutBounds(string UsePilotCode = "")
        {
            return SqlHelper.SqlHelper.sqlRundownBaseOutbounds(conn(), UsePilotCode);
        }


        [DnnAuthorize()]
        [HttpGet]
        public List<RundownProjectedInbounds> RundownBaseProjectedInbounds()
        {
            return SqlHelper.SqlHelper.sqlRundownBaseProjectedInbounds(conn());
        }


        [DnnAuthorize()]
        [HttpGet]
        public List<RundownProjectedOutbounds> RundownBaseProjectedOutbounds()
        {
            return SqlHelper.SqlHelper.sqlRundownBaseProjectedOutbounds(conn());
        }


        [DnnAuthorize()]
        [HttpGet]
        public List<RundownProjectedShifts> RundownBaseProjectedShifts()
        {
            return SqlHelper.SqlHelper.sqlRundownBaseProjectedShifts(conn());
        }

        [DnnAuthorize()]
        [HttpGet]
        public List<Details> VesselDetailsInbound(string callSign)
        {
            return SqlHelper.SqlHelper.VesselDetailsInbound(conn(), callSign);
        }


        [DnnAuthorize()]
        [HttpGet]
        public List<Note_Kendo> NoteBase(string ShowNotes, string ShowSeaReport, string ShowClosures)
        {
            return SqlHelper.SqlHelper.sqlNoteBase(conn(), ShowNotes, ShowSeaReport, ShowClosures);
        }

        [DnnAuthorize()]
        [HttpGet]
        public List<RundownCsHarborShifts> RundownBaseCsHarborShifts(string UsePilotCode = "")
        {
            return SqlHelper.SqlHelper.sqlRundownBaseCsHarborShifts(conn(), UsePilotCode);
        }


        [DnnAuthorize()]
        [HttpGet]
        public List<Alert> AlertBase(string ShowLadder, string ShowNotices)
        {
            return SqlHelper.SqlHelper.sqlAlertBase(conn(), ShowLadder, ShowNotices);
        }


        [DnnAuthorize()]
        [HttpGet]
        public List<Pilot_Kendo> WorkingPilotsBase()
        {
            return SqlHelper.SqlHelper.sqlWorkingPilotsBase(conn());
        }


        [DnnAuthorize()]
        [HttpGet]
        public List<Pilot_Watch_Slate> Watch_Slate_PilotsBase()
        {
            return SqlHelper.SqlHelper.sql_Watch_Slate_PilotsBase(conn());
        }

        [DnnAuthorize()]
        [HttpGet]
        public List<Pilot_Unavailable> Unavailable_PilotsBase()
        {
            return SqlHelper.SqlHelper.sql_Pilot_UnavailablesBase(conn());
        }

        [DnnAuthorize()]
        [HttpGet]
        public List<Rundown_Docked> Rundown_Dockeds(string DaysBack)
        {
            return SqlHelper.SqlHelper.sql_Rundown_Dockeds(conn(), DaysBack);
        }

        #endregion


        #region "Data Calls"

        protected PsqlConnection conn()
        {
            //Local
            var con = new PsqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SabinePervasiveConnection"].ConnectionString);

            //Sabine

            //var con = new PsqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SabineConnectionString"].ConnectionString);

            return con;

        }

        #endregion


        #region "Helpers"
        protected string ProperCase(string input)
        {

            // Defines the string with mixed casing.
            string myString = input;                                       //; "5CARNIVAL LEGEND" //[5]    "116-SHAFFER, D. L. "

            // Creates a TextInfo based on the "en-US" culture.
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

            // Changes a string to lowercase.
            int plevel;
            bool IsNumeric = int.TryParse(myString.Substring(0, 1), out plevel);
            //try




            string inString = myTI.ToLower(myString);
            string outString = string.Empty;
            char[] chars = inString.ToCharArray();

            //Always try upper start
            string subString = chars[0].ToString();


            subString = myTI.ToUpper(subString);

            char[] subChars = subString.ToCharArray();

            chars[0] = subChars[0];


            //Loop through once to find things for upper case
            if (IsNumeric)
            {
                subString = chars[1].ToString();


                subString = myTI.ToUpper(subString);

                subChars = subString.ToCharArray();

                chars[1] = subChars[0];

            }


            for (int i = 1; i < chars.Length; i++)
            {

                //Name will Follow Pilot number
                if (chars[i].ToString() == "-" || chars[i].ToString() == "(")
                {

                    subString = chars[i + 1].ToString();


                    subString = myTI.ToUpper(subString);

                    subChars = subString.ToCharArray();

                    chars[i + 1] = subChars[0];
                }

                //Space marks a new word
                if ((chars[i].ToString() == " " && (i + 1) < chars.Length))
                {

                    subString = chars[i + 1].ToString();


                    subString = myTI.ToUpper(subString);

                    subChars = subString.ToCharArray();

                    chars[i + 1] = subChars[0];
                }


                //Marks an initial
                if (chars[i].ToString() == ".")
                {

                    subString = chars[i - 1].ToString();


                    subString = myTI.ToUpper(subString);

                    subChars = subString.ToCharArray();

                    chars[i - 1] = subChars[0];
                }



            }
            for (int i = 0; i < chars.Length; i++)
            {
                outString = outString += chars[i].ToString();
            }


            outString = outString.Replace("Atb", "ATB");


            return outString;




        }




        #endregion

    }
}

﻿using System;

namespace Coesolutions.Modules.DemoDataService.Models
{
    public class Pilot_Kendo
    {
        public static int myIndex = 1;
        public static int myWorkingIndex = 1;

        public Pilot_Kendo(Pervasive.Data.SqlClient.PsqlDataReader reader)
        {

            string addTransIconStart;
            if (!System.Environment.MachineName.ToUpper().Contains("KEVIN"))
            {
                addTransIconStart = @"<img src=""/riversafe/DesktopModules/DemoDataService/Images/Car20px.png"" alt=""(T)"">";
            }
            else
            {
                addTransIconStart = @"<img src=""/DesktopModules/DemoDataService/Images/Car20px.png"" alt=""(T)"">";
            }
            string addTransIconEnd = @"";
            string addRestEarnedFmtStart = @"<font style=""color:White; background-color:Black;"">";
            string addRestEarnedFmtEnd = @"</font>";
            
            this.slateIndex = myIndex;

            string pName = string.Empty;

            //Pilot reverse text and car icon removed per request
            pName = reader["PilotName"].ToString().Trim();
            if (Convert.ToBoolean(reader["PilotRestEarned"]) == true)
            {
                //Changes appearance of pilot as span.  requires settings EncodeHtml="False" on DevExpress column
                //pName = addRestEarnedFmtStart + reader["PilotName"].ToString().Trim() + addRestEarnedFmtEnd;
                this.PilotRestEarned = true;
            }
            else
            {
                //pName = reader["PilotName"].ToString().Trim();
                this.PilotRestEarned = false;
            }

            if (Convert.ToBoolean(reader["PilotHasTransportation"]) == true)
            {
                //Add a little space to it as well
                //pName = addTransIconStart + " " + pName;
                this.PilotHasTransportation = true;

            }
            else
            {
                this.PilotHasTransportation = false;
            }

            this.RestPeriodStartDateTime = Convert.ToDateTime(reader["RestPeriodStartDateTime"]);
            this.RestPeriodEndDateTime = Convert.ToDateTime(reader["RestPeriodEndDateTime"]);
            //Add logic for new working index that only advances for pilots not on rest
            if (this.RestPeriodStartDateTime < DateTime.Now && DateTime.Now < this.RestPeriodEndDateTime)
            {
                this.RestPeriodDisplay = this.RestPeriodEndDateTime.ToString("HH:mm");
                this.strWorkingIndex = "";
            }
            else
            {
                //advance working index
                this.RestPeriodDisplay = "";
                this.strWorkingIndex = myWorkingIndex.ToString();
                myWorkingIndex += 1;
            }
            this.PilotName = pName;

            myIndex++;

        }
        #region Properties

        public int slateIndex { get; set; }
        public string strWorkingIndex { get; set; }
        public bool HighlightedYellow { get; set; }
        public bool IsOnRest { get; set; }
        public string SlateInfo { get; set; }
        public string AssignedYesNo { get; set; }
        public TimeSpan FirstPilotFinishTime { get; set; } // from rundown
        public DateTime FinishDate { get; set; } // from rundown
        public double FirstPilotCode { get; set; } // from rundown
        public int HoursSinceLastFinish { get; set; }
        public int HoursTilRestPeriodEnd { get; set; }
        public bool PilotRestEarned { get; set; }
        public bool PilotHasTransportation { get; set; }
        public DateTime LastFinishDate { get; set; }
        public DateTime LastFinishDateTime { get; set; }

        public TimeSpan LastFinishTime { get; set; }

        public double PilotCode { get; set; }

        public string PilotName { get; set; }
        public int PilotsCurrentRestHours { get; set; }

        public DateTime RestPeriodStartDateTime { get; set; }
        public DateTime RestPeriodEndDateTime { get; set; }
        public String RestPeriodDisplay { get; set; }
        public string RestPeriodDeclineYesNo { get; set; }

        public int RestrictedDraft { get; set; }
        public int RestrictedDwt { get; set; }
        public string RestrictedYesNo { get; set; }
        public string PilotGroup { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }


        public double DataN2 { get; set; }
        public string HireExtraYesNo { get; set; }
        public string MoneyManType1 { get; set; }
        public string MoneyManType2 { get; set; }
        public string MoneyManType3 { get; set; }
        public string MoneyManType4 { get; set; }

        public bool IsMoneyMan { get; set; }
        public int ReturningOrder { get; set; }

        public string SmsEmailAddress { get; set; }

        #endregion
    }
}

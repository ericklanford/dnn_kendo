﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coesolutions.Modules.DemoDataService.Models
{
    public class Details
    {
        public string RowHeader { get; set; }
        public string RowValue { get; set; }
    }
}
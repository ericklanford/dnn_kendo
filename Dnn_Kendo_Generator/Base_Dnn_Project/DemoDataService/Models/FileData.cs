﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coesolutions.Modules.DemoDataService.Models
{

    public class FileData
    {
        public string ContentType { get; set; }
        public string Base64 { get; set; }
        public string FileName { get; set; }
    }
}
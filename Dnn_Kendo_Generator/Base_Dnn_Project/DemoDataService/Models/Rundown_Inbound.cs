﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetNuke.Entities.Modules;


namespace Coesolutions.Modules.DemoDataService.Models
{
    public class Rundown_Inbound
    {
        public Rundown_Inbound(Pervasive.Data.SqlClient.PsqlDataReader reader, string UsePilotCode)
        {
            bool pcodePrepend = false;

            if (UsePilotCode == "True")
            {
                pcodePrepend = true;
            }

            //Pilot string transportation enhancemennts
            //Configure version for local and deployed server using riversafe as part of url
            
            string addTransIconStart;
            if (!System.Environment.MachineName.ToUpper().Contains("KEVIN"))
            {
                addTransIconStart = @"<img src=""/riversafe/DesktopModules/DemoDataService/Images/Car20px.png"" alt=""(T)"">";
            }
            else
            {
                addTransIconStart = @"<img src=""/DesktopModules/DemoDataService/Images/Car20px.png"" alt=""(T)"">";
            }
            string addTransIconEnd = @"";
            string addRestEarnedFmtStart = @"<font style=""color:White; background-color:Black;"">";
            string addRestEarnedFmtEnd = @"</font>";


            //Prepend the tug name to the vessel string
            if (reader["TugPrimary"].ToString().Trim().Length > 0)
            {
                this.VesselName = reader["TugPrimary"].ToString().Trim()
                    + " - <br/> " + reader["VesselName"].ToString().Trim();
            }
            else
            {
                this.VesselName = reader["VesselName"].ToString().Trim();
            }
            this.CallSign = reader["CallSign"].ToString();

            //Pilots
            //this.FirstPilotCode = Convert.ToDouble(reader["FirstPilotCode"]);

            string pName = string.Empty;
            if (pcodePrepend)
            {
                if (Convert.ToInt32(reader["PilotCode"]) > 0)
                {
                    //this.CombinedPilots = this.CombinedPilots + " - <br/> " + reader["SecondPilotCode"].ToString();
                    this.PilotName = string.Format("({0}) {1}", reader["PilotCode"].ToString(), reader["PilotName"].ToString().Trim());
                }
                else
                {
                    this.PilotName = reader["PilotName"].ToString().Trim();
                }
                if (Convert.ToInt32(reader["SecondPilotCode"]) > 0)
                {
                    this.SecondPilotName = string.Format("({0}) {1}", reader["SecondPilotCode"].ToString(), reader["SecondPilotName"].ToString().Trim());
                }
                else
                {
                    this.SecondPilotName = reader["SecondPilotName"].ToString().Trim();

                }
                if (Convert.ToInt32(reader["ThirdPilotCode"]) > 0)
                {
                    this.ThirdPilotName = string.Format("({0}) {1}", reader["ThirdPilotCode"].ToString(), reader["ThirdPilotName"].ToString().Trim());
                }
                else
                {
                    this.ThirdPilotName = reader["ThirdPilotName"].ToString().Trim();
                }

            }
            else
            {
                this.PilotName = reader["PilotName"].ToString().Trim();
                this.SecondPilotName = reader["SecondPilotName"].ToString().Trim();
                this.ThirdPilotName = reader["ThirdPilotName"].ToString().Trim();

                this.CombinedPilots = reader["PilotCode"].ToString();
            }

            if (Convert.ToBoolean(reader["PilotRestEarned"]) == true)
            {
                //Changes appearance of pilot as span.  requires settings EncodeHtml="False" on DevExpress column
                pName = addRestEarnedFmtStart + this.PilotName + addRestEarnedFmtEnd;
            }
            else
            {
                pName = this.PilotName;
            }

            if (Convert.ToBoolean(reader["PilotHasTransportation"]) == true)
            {
                //Add a little space to it as well
                pName = addTransIconStart + " " + pName;

            }

            this.PilotName = pName;
            this.CombinedPilots = this.PilotName;

            string pName2 = string.Empty;
            if (Convert.ToBoolean(reader["SecondPilotRestEarned"]) == true)
            {
                //Changes appearance of Second as span.  requires settings EncodeHtml="False" on DevExpress column
                pName2 = addRestEarnedFmtStart + this.SecondPilotName + addRestEarnedFmtEnd;
            }
            else
            {
                pName2 = this.SecondPilotName;
            }

            if (Convert.ToBoolean(reader["SecondPilotHasTransportation"]) == true)
            {
                //Add a little space to it as well
                pName2 = addTransIconStart + " " + pName2;
            }

            this.SecondPilotName = pName2;
            if (this.SecondPilotName.Length > 0)
            {
                this.CombinedPilots = this.CombinedPilots + " - <br/> " + this.SecondPilotName;
            }



            string pName3 = string.Empty;
            if (Convert.ToBoolean(reader["ThirdPilotRestEarned"]) == true)
            {
                //Changes appearance of Third as span.  requires settings EncodeHtml="False" on DevExpress column
                pName3 = addRestEarnedFmtStart + this.ThirdPilotName + addRestEarnedFmtEnd;
            }
            else
            {
                pName3 = this.ThirdPilotName;
            }

            if (Convert.ToBoolean(reader["ThirdPilotHasTransportation"]) == true)
            {
                //Add a little space to it as well
                pName3 = addTransIconStart + " " + pName3;
            }

            this.ThirdPilotName = pName3;
            if (this.ThirdPilotName.Length > 0)
            {
                this.CombinedPilots = this.CombinedPilots + " - <br/> " + this.ThirdPilotName;
            }

            //Add apprentices to combined string 
            if (reader["Apprentice1"].ToString().Trim().Length > 0)
            {
                this.CombinedPilots = this.CombinedPilots + " - <br/> " + reader["Apprentice1"].ToString().Trim();
            }
            if (reader["Apprentice2"].ToString().Trim().Length > 0)
            {
                this.CombinedPilots = this.CombinedPilots + " - <br/> " + reader["Apprentice2"].ToString().Trim();
            }
            if (reader["Apprentice3"].ToString().Trim().Length > 0)
            {
                this.CombinedPilots = this.CombinedPilots + " - <br/> " + reader["Apprentice3"].ToString().Trim();
            }
            if (reader["Apprentice4"].ToString().Trim().Length > 0)
            {
                this.CombinedPilots = this.CombinedPilots + " - <br/> " + reader["Apprentice4"].ToString().Trim();
            }

            //Draft as formatted string or - if not entered
            string Draft = string.Empty;
            if (Convert.ToInt32(reader["RundownDraftFeet"]) > 0)
            {
                Draft += Convert.ToInt32(reader["RundownDraftFeet"]).ToString() + "'";
                Draft += Convert.ToInt32(reader["RundownDraftInches"]).ToString() + "\"";
            }
            else
            {
                Draft = " - ";
            }

            this.CombinedDraft = Draft;

            this.CodesDescriptionFrom = reader["CodesDescriptionFrom"].ToString();
            this.CodesDescriptionTo = reader["CodesDescriptionTo"].ToString();

            //Handle Dates as string for display

            //Show nothing for default date time value
            this.OrderDateTime = Convert.ToDateTime(reader["OrderDateTime"]);
            if (this.OrderDateTime.ToString("HHmm") == "0000")
            {
                this.OrderDateTimeString = "";
            }
            else
            {
                if (this.OrderDateTime.ToShortDateString() == DateTime.Now.ToShortDateString())
                {
                    //Skip showing the date part
                    this.OrderDateTimeString = this.OrderDateTime.ToString("  HH:mm");
                }
                else
                {
                    //show the date part
                    this.OrderDateTimeString = this.OrderDateTime.ToString("dd/HH:mm");
                }
            }
            //The following times will not use a date source just a PSQL Time.
            //Time span diff corrects the .net convert function 
            //assigning the current date to the dateteime
            DateTime def = new System.DateTime();
            System.TimeSpan diff = DateTime.Today.Subtract(def);
            this.SbbTime = Convert.ToDateTime(reader["SbbTime"]);
            this.SbbTime = this.SbbTime.Subtract(diff);
            this.C930Time = Convert.ToDateTime(reader["C930Time"]);
            this.C930Time = this.C930Time.Subtract(diff);
            this.MsqptTime = Convert.ToDateTime(reader["MsqptTime"]);
            this.MsqptTime = this.MsqptTime.Subtract(diff);
            this.NriTime = Convert.ToDateTime(reader["NriTime"]);
            this.NriTime = this.NriTime.Subtract(diff);
            this.C09Time = Convert.ToDateTime(reader["C09Time"]);
            this.C09Time = this.C09Time.Subtract(diff);
            this.Sun5Time = Convert.ToDateTime(reader["Sun5Time"]);
            this.Sun5Time = this.Sun5Time.Subtract(diff);
            this.ClearTime = Convert.ToDateTime(reader["ClearTime"]);
            this.ClearTime = this.ClearTime.Subtract(diff);
            this.Confirmed = Convert.ToBoolean(reader["Confirmed"]);
            if (this.Confirmed)
            {
                this.Conf = "Confirmed";
            }
            else
            {
                this.Conf = "Due";
            }
            this.Estimated = Convert.ToBoolean(reader["Estimated"]);
            if (this.Estimated)
            {
                this.Est = "Estimated";
            }
            else
            {
                this.Est = "";
            }
            this.RundownNotes = reader["RundownNotes"].ToString().Trim();

            //Try to get a time out of time estimate and resort to ordertime as backup
            if (reader["TimeEstimate"].ToString().Contains("?"))
            {
                this.OnBoardDateTime = this.OrderDateTime;
            }
            else
            {
                //Get the parts of the string for testing
                string strHours = reader["TimeEstimate"].ToString().Substring(0, 2);
                string strMinutes = reader["TimeEstimate"].ToString().Substring(2, 2);
                try
                {
                    int intHours = Convert.ToInt32(strHours);
                    int intMinutes = Convert.ToInt32(strMinutes);
                    if (intHours >= 0 && intHours < 24 && intMinutes >= 0 && intMinutes < 60)
                    {

                        DateTime myBoardDate = Convert.ToDateTime(reader["EstDateOnBoard"]);
                        myBoardDate = myBoardDate.AddHours(intHours);
                        myBoardDate = myBoardDate.AddMinutes(intMinutes);
                        this.OnBoardDateTime = myBoardDate;
                    }
                    else
                    {
                        this.OnBoardDateTime = this.OrderDateTime;
                    }
                }
                catch (Exception ex)
                {
                    this.OnBoardDateTime = this.OrderDateTime;
                }
            }
            this.EstDateOnBoard = Convert.ToDateTime(reader["EstDateOnBoard"]);

            //Restrictions "VesselHoldyesNo"
            this.VesselHoldYesNo = reader["VesselHoldyesNo"].ToString().Trim();
            this.VesselHoldDescription = reader["VesselHoldDescription"].ToString().Trim();
            this.VesselRestriction = reader["VesselRestriction"].ToString().Trim();


            //Setup for isDaylightOnly
            this.VesselBeam = Convert.ToDouble(reader["VesselBeam"]);
            this.VesselLoa = Convert.ToDouble(reader["VesselLoa"]);
            this.VesselDwt = Convert.ToInt32(reader["VesselDwt"]);
            if (this.VesselBeam > 125 || this.VesselLoa > 875 || this.VesselDwt > 85000)
            {
                this.IsDaylightOnlyJob = true;
                this.IsTwoPilotJob = false;
            }
            else
            {
                this.IsDaylightOnlyJob = false;
                if (this.VesselBeam > 120 || this.VesselLoa > 860)
                {
                    this.IsTwoPilotJob = true;
                }
                else
                {
                    this.IsTwoPilotJob = false;
                }
            }
            this.AgentName = reader["AgentName"].ToString();
            this.VesselFlag = reader["VesselFlag"].ToString();            
        }
        #region Properties

        public bool PilotRestEarned { get; set; }
        public bool SecondPilotRestEarned { get; set; }
        public bool ThirdPilotRestEarned { get; set; }

        //deprecated
        public bool HasTransportation { get; set; }

        public bool PilotHasTransportation { get; set; }
        public bool SecondPilotHasTransportation { get; set; }
        public bool ThirdPilotHasTransportation { get; set; }

        public DateTime SecondPilotFinishTime { get; set; }

        public string AgentCell1 { get; set; }

        public string AgentCode { get; set; }

        public string AgentContactsHtml { get; set; }

        public string AgentName { get; set; }


        public string AgentPager1 { get; set; }


        public string AgentPhone { get; set; }


        public string AgentPhone1 { get; set; }


        public double AirDraftFeet { get; set; }


        public double AirDraftInches { get; set; }

        public DateTime ArrivalTime { get; set; }


        public DateTime AssignedDate { get; set; }


        public DateTime AssignedDateTime { get; set; }
        public string AssignedDateTimeString { get; set; }


        public DateTime AssignedTime { get; set; }

        public DateTime C09Time { get; set; }


        public DateTime C6DateTime { get; set; }
        public string C6DateTimeString { get; set; }

        public DateTime C930Time { get; set; }
        public DateTime C9_30_DateTime { get; set; }
        public string C9_30_DateTimeString { get; set; }


        public DateTime CallDateTime { get; set; }
        public string CallDateTimeString { get; set; }


        public string CallSign { get; set; }

        public DateTime CallTime { get; set; }


        public DateTime ChangeDate { get; set; }

        public DateTime ChangeDateTime { get; set; }
        public string ChangeDateTimeString { get; set; }


        public DateTime ChangeTime { get; set; }
        public string ChannelCodeFrom { get; set; }
        public string ChannelCodeTo { get; set; }
        public double ChannelFirstVesselDwtLimitFrom { get; set; }
        public double ChannelFirstVesselDwtLimitTo { get; set; }
        public int ChannelMaxBeamFrom { get; set; }
        public int ChannelMaxBeamTo { get; set; }
        public string ChannelNameFrom { get; set; }
        public string ChannelNameTo { get; set; }
        public double ChannelSecondVesselDwtLimitFrom { get; set; }
        public double ChannelSecondVesselDwtLimitTo { get; set; }

        public DateTime ClearTime { get; set; }
        public string CodesChannelFrom { get; set; }
        public string CodesChannelTo { get; set; }


        public string CodesDescriptionFrom { get; set; }


        public string CodesDescriptionTo { get; set; }


        public string CodesPhoneNotesFrom { get; set; }


        public string CodesPhoneNotesTo { get; set; }


        public string CombinedAirDraft { get; set; }


        public string CombinedDraft { get; set; }
        public string Conf { get; set; }
        public bool Confirmed { get; set; }
        public string Est { get; set; }
        public bool Estimated { get; set; }


        public string Direction { get; set; }


        public DateTime EstDateOnBoard { get; set; }

        public string ExtraYesNo { get; set; }

        public DateTime FinishDate { get; set; }

        public double FirstPilotCode { get; set; }


        public string FromMileageCode { get; set; }


        public DateTime IcwwDateTime { get; set; }
        public bool IsDaylightOnlyJob { get; set; }


        public bool IsOlder { get; set; }
        public bool IsTwoPilotJob { get; set; }


        public DateTime JobFinishDateTime { get; set; }
        public string JobFinishDateTimeString { get; set; }


        public string LastChange { get; set; }


        public DateTime LastLineDateTime { get; set; }
        public string LastLineDateTimeString { get; set; }


        public string LcStatus { get; set; }


        public string LineHandler { get; set; }


        public double MileNumFrom { get; set; }


        public double MileNumTo { get; set; }
        public DateTime MsqptTime { get; set; }
        public DateTime NriTime { get; set; }


        public DateTime OnBoardDateTime { get; set; }
        public string OnBoardDateTimeString { get; set; }

        public DateTime OnBoardDate { get; set; }
        public DateTime OrderDateTime { get; set; }
        public string OrderDateTimeString { get; set; }
        public DateTime OrderTime { get; set; }


        public DateTime PilotAboardDateTime { get; set; }
        public string PilotAboardDateTimeString { get; set; }
        public DateTime PilotFinishTime { get; set; }

        public double PilotCode { get; set; }
        public string PilotName { get; set; }
        public string PilotPhone { get; set; }

        public int RundownDraftFeet { get; set; }

        public string SmsMessage { get; set; }
        public string SmsMessageFrom { get; set; }
        public string SmsPilotName { get; set; }
        public int RundownDraftInches { get; set; }


        public double RundownDwt { get; set; }
        public double RundownExtremeBeam { get; set; }


        public Guid RundownId { get; set; }
        public double SmsPilotCode { get; set; }

        public double RundownLoa { get; set; }


        public string RundownNotes { get; set; }


        public string RundownOrderAgentCode { get; set; }
        public DateTime SbbTime { get; set; }

        public double SecondPilotCode { get; set; }
        public string SecondPilotName { get; set; }
        public string SecondPilotPhone { get; set; }


        public string SideTo { get; set; }
        public DateTime Sun5Time { get; set; }
        public double ThirdPilotCode { get; set; }
        public string ThirdPilotName { get; set; }
        public string ThirdPilotPhone { get; set; }


        public string TimeEstimate { get; set; }


        public string ToMileageCode { get; set; }


        public string TugPrimary { get; set; }


        public string TurnStatus { get; set; }


        public double VesselBeam { get; set; }


        public double VesselBredthFeet { get; set; }


        public int VesselDwt { get; set; }


        public string VesselFlag { get; set; }

        public string VesselHoldYesNo { get; set; }
        public string VesselHoldDescription { get; set; }
        public DateTime VesselHoldDate { get; set; }
        public string VesselImo { get; set; }


        public string VesselLastPort { get; set; }


        public string VesselLink { get; set; }


        public double VesselLoa { get; set; }


        public string VesselName { get; set; }


        public string VesselNote { get; set; }


        public string VesselRestriction { get; set; }
        public DateTime VesselRestrictionDate { get; set; }


        public string VesselType { get; set; }


        public string VisibleStatus { get; set; }

        public string PilotSmsEmailAddress { get; set; }

        public string SecondPilotSmsEmailAddress { get; set; }

        public string ThirdPilotSmsEmailAddress { get; set; }


        public string CombinedPilots { get; set; }
        #endregion
    }
}
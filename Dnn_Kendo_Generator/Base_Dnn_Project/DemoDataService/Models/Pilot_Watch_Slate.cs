﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coesolutions.Modules.DemoDataService.Models
{
    public class Pilot_Watch_Slate
    {
        public static int myIndex = 1;

        public Pilot_Watch_Slate(Pervasive.Data.SqlClient.PsqlDataReader reader)
        {
            string addMoneyManIconStart;
            string addMoneyManIconStartDeputy;
            if (!System.Environment.MachineName.ToUpper().Contains("KEVIN"))
            {
                addMoneyManIconStart = @"<img src=""/riversafe/DesktopModules/DemoDataService/Images/Circle-Icon-Dollar-Sign.png"" alt=""(T)""  style=""width:20px;height:20px;border:0"">";
            }
            else
            {
                addMoneyManIconStart = @"<img src=""/DesktopModules/DemoDataService/Images/Circle-Icon-Dollar-Sign.png"" alt=""(T)"" style=""width:20px;height:20px;border:0"">";
            }
            if (!System.Environment.MachineName.ToUpper().Contains("KEVIN"))
            {
                addMoneyManIconStartDeputy = @"<img src=""/riversafe/DesktopModules/DemoDataService/Images/Circle-Icon-Dollar-Sign-Deputy.png"" alt=""(T)""  style=""width:20px;height:20px;border:0"">";
            }
            else
            {
                addMoneyManIconStartDeputy = @"<img src=""/DesktopModules/DemoDataService/Images/Circle-Icon-Dollar-Sign-Deputy.png"" alt=""(T)"" style=""width:20px;height:20px;border:0"">";
            }
            string addTransIconEnd = @"";
            string addDNCIconStart;
            if (!System.Environment.MachineName.ToUpper().Contains("KEVIN"))
            {
                addDNCIconStart = @"<img src=""/riversafe/DesktopModules/DemoDataService/Images/do-not-call-icon-20.png"" alt=""(T)""  style=""width:20px;height:20px;border:0"">";
            }
            else
            {
                addDNCIconStart = @"<img src=""/DesktopModules/DemoDataService/Images/do-not-call-icon.png"" alt=""(T)""  style=""width:20px;height:20px;border:0"">";
            }

            string addRestEarnedFmtStart = @"<font style=""color:White; background-color:Black;"">";
            string addRestEarnedFmtEnd = @"</font>";




            slateIndex = myIndex;

            if (reader["MoneyManType1"].ToString().Trim() == "Y"
                || reader["MoneyManType2"].ToString().Trim() == "Y"
                 || reader["MoneyManType3"].ToString().Trim() == "Y"
                 || reader["MoneyManType4"].ToString().Trim() == "Y")
            {
                IsMoneyMan = true;
            }
            else
            {
                IsMoneyMan = false;
            }

            string pName = string.Empty;
            //Pilot reverse text and car icon removed per request
            pName = reader["PilotName"].ToString().Trim();
            if (Convert.ToBoolean(reader["PilotRestEarned"]) == true)
            {
                //Changes appearance of pilot as span.  requires settings EncodeHtml="False" on DevExpress column
                //pName = addRestEarnedFmtStart + reader["PilotName"].ToString().Trim() + addRestEarnedFmtEnd;
                PilotRestEarned = true;
            }
            else
            {
                //pName = reader["PilotName"].ToString().Trim();
                PilotRestEarned = false;
            }
            //pName = reader["PilotName"].ToString().Trim();
            PilotGroup = reader["PilotGroup"].ToString().Trim();

            if (IsMoneyMan)
            {

                if (PilotGroup.Contains("C") || PilotGroup.Contains("D"))
                {
                    PilotName = addMoneyManIconStartDeputy + pName;
                }
                else
                {
                    PilotName = addMoneyManIconStart + pName;
                }
            }
            else
            {
                PilotName = pName;
            }


            //if (Convert.ToBoolean(reader["PilotHasTransportation"]) == true)
            //{
            //    //Add a little space to it as well
            //    pName = addMoneyManIconStart + " " + pName;
            //    PilotHasTransportation = true;

            //}
            //else
            //{
            //    PilotHasTransportation = false;
            //}
            RestPeriodStartDateTime = Convert.ToDateTime(reader["RestPeriodStartDateTime"]);
            RestPeriodEndDateTime = Convert.ToDateTime(reader["RestPeriodEndDateTime"]);
            if (RestPeriodStartDateTime < DateTime.Now && DateTime.Now < RestPeriodEndDateTime)
            {
                RestPeriodDisplay = RestPeriodEndDateTime.ToString("HH:mm");
            }
            else
            {
                RestPeriodDisplay = "";
            }

            ReturningOrder = Convert.ToInt32(reader["ReturningOrder"]);


            if (reader["HireExtraYesNo"].ToString().Contains("N"))
            {
                HireExtraYesNo = addDNCIconStart + " Do Not Call";
            }
            else
            {
                HireExtraYesNo = "";
            }



            myIndex++;

        }


        #region Properties

        public int slateIndex { get; set; }
        public string strWorkingIndex { get; set; }

        public bool HighlightedYellow { get; set; }
        public bool IsOnRest { get; set; }
        public string SlateInfo { get; set; }
        public string AssignedYesNo { get; set; }
        public TimeSpan FirstPilotFinishTime { get; set; } // from rundown
        public DateTime FinishDate { get; set; } // from rundown
        public double FirstPilotCode { get; set; } // from rundown
        public int HoursSinceLastFinish { get; set; }
        public int HoursTilRestPeriodEnd { get; set; }
        public bool PilotRestEarned { get; set; }
        public bool PilotHasTransportation { get; set; }
        public DateTime LastFinishDate { get; set; }
        public DateTime LastFinishDateTime { get; set; }

        public TimeSpan LastFinishTime { get; set; }

        public double PilotCode { get; set; }

        public string PilotName { get; set; }
        public int PilotsCurrentRestHours { get; set; }

        public DateTime RestPeriodStartDateTime { get; set; }
        public DateTime RestPeriodEndDateTime { get; set; }
        public String RestPeriodDisplay { get; set; }
        public string RestPeriodDeclineYesNo { get; set; }

        public int RestrictedDraft { get; set; }
        public int RestrictedDwt { get; set; }
        public string RestrictedYesNo { get; set; }
        public string PilotGroup { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }


        public double DataN2 { get; set; }
        public string HireExtraYesNo { get; set; }
        public string MoneyManType1 { get; set; }
        public string MoneyManType2 { get; set; }
        public string MoneyManType3 { get; set; }
        public string MoneyManType4 { get; set; }

        public bool IsMoneyMan { get; set; }
        public int ReturningOrder { get; set; }

        public string SmsEmailAddress { get; set; }

        #endregion
    }
}
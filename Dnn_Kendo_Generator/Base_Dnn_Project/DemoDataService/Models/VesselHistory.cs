﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coesolutions.Modules.DemoDataService.Models
{
    public class VesselHistory
    {
        public string VoyageId { get; internal set; }
        public string CompletionDateTimeString { get; set; }
        public string Draft { get; internal set; }
        public string ElapsedTimeString { get; internal set; }
        public string EndLoc { get; internal set; }
        public string MovementStatus { get; internal set; }
        public string StartDateTimeString { get; set; }
        public string StartLoc { get; internal set; }
        public string TransitID { get; internal set; }
    }
}

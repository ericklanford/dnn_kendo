﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coesolutions.Modules.DemoDataService.Models
{
    public class Alert
    {
        public Alert(Pervasive.Data.SqlClient.PsqlDataReader reader)
        {
            Priority = Convert.ToInt32(reader["Priority"]);
            Note1 = reader["Note1"].ToString();
            LastChangeDate = Convert.ToDateTime(reader["LastChangeDate"]);
            LastChange = reader["LastChange"].ToString();
        }

        public int Priority { get; set; }
        public string NoteType { get; set; }
        public string Note1 { get; set; }
        public string LastChange { get; set; }
        public DateTime LastChangeDate { get; set; }
    }
}

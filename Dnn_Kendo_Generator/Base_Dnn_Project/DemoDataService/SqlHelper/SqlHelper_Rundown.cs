﻿using Coesolutions.Modules.DemoDataService.Models;
using DotNetNuke.Services.Exceptions;
using Pervasive.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Coesolutions.Modules.DemoDataService.SqlHelper
{
    public static partial class SqlHelper
    {
        public static List<Rundown_Inbound> sqlRundownBase(PsqlConnection conn, string UsePilotCode)
        {
            var intList = new List<Rundown_Inbound>();

            #region cmdString
            var cmdString = @"SELECT Est__Date_on_Board as EstDateOnBoard, Time_Estimate as TimeEstimate, Call_Sign as CallSign
            , Vessel_Name as VesselName, MileNumFrom, Confirmed,
            From_Mileage_Code as FromMileageCode, MileNumTo, To_Mileage_Code as ToMileageCode, Draft_Footage as RundownDraftFeet, Draft_Inches as RundownDraftInches,
            ChangeDate, ChangeTime, LastChange, Extra_YN as ExtraYesNo, Call_Time as CallTime, Finish_Date as FinishDate, Order_Time as OrderTime,
            AirDraftFt as AirDraftFeet, AirDraftInches, Rundown_Table.Notes as RundownNotes, Order_Agent_Code as RundownOrderAgentCode,C09_Time as C09Time,
            NRI_Time as NriTime,Sun5_Time as Sun5Time, Rundown_Table.ETA as ClearTime, SBB_Time as SbbTime,C9_30_Time as C930Time,MSQPT_Time as MsqptTime,
            Assigned_Date as AssignedDate, Assigned_Time as AssignedTime, Turn_Status_Pending as TurnStatus,First_Pilot_Code as FirstPilotCode, 
            Second_Pilot_Code as SecondPilotCode, Third_Pilot_Code as ThirdPilotCode, Tug_Primary as TugPrimary,
            Extreme_Beam as RundownExtremeBeam, On_Board_Date as OnBoardDate,Second_Pilot_Finish as SecondPilotFinishTime,
            Vessels.Restriction as VesselRestriction, Vessels.DWT as VesselDwt, Vessels.LOAFeet as VesselLoa, Vessels.Type as VesselType, 
            Vessels.BredthFeet as VesselBredthFeet, Vessels.Flag as VesselFlag, Vessels.Vessel_Note as VesselNote, Vessels.HoldDescription as VesselHoldDescription,
            Vessels.LR_IMO_Ship_Number as VesselImo, Vessels.Extreme_Beam as VesselBeam,Vessels.HoldYN as VesselHoldYesNo, Vessels.Restriction_Date as VesselRestrictionDate,
            Vessels.Hold_Date as VesselHoldDate,
            Agent_Codes.AgentName, Agent_Codes.Phone as AgentPhone, Agent_Codes.Phone_1 AS AgentPhone1, Agent_Codes.Cell_1 AS AgentCell1, 
            Agent_Codes.Pager_1 AS AgentPager1, Agent_Codes.AgentCode,
            CodesFrom.Phone_Notes as CodesPhoneNotesFrom, CodesFrom.Description1 as CodesDescriptionFrom, CodesFrom.Channel as CodesChannelFrom,
            CodesTo.Phone_Notes as CodesPhoneNotesTo, CodesTo.Description1 as CodesDescriptionTo, CodesTo.Channel as CodesChannelTo,

            ChannelsFrom.Channel_Name as ChannelNameFrom, ChannelsFrom.Max_Combined_Beam as ChannelMaxBeamFrom, ChannelsFrom.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitFrom,
            ChannelsFrom.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitFrom,ChannelsFrom.Channel_Code as ChannelCodeFrom, 

            ChannelsTo.Channel_Name as ChannelNameTo, ChannelsTo.Max_Combined_Beam as ChannelMaxBeamTo, ChannelsTo.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitTo,
            ChannelsTo.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitTo,ChannelsTo.Channel_Code as ChannelCodeTo,

            Pilots.PilotName as PilotName, Pilots.Phone1 as PilotPhone, Pilots.PilotCode, Pilots.eNotifyAddress as PilotSmsEmailAddress,
            CAST((IF(Pilots.Filler3='Yes',1,0)) AS Bit) as PilotRestEarned,
            CAST((IF(Pilots.DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation,

            Pilots2.PilotName as SecondPilotName, Pilots2.Phone1 as SecondPilotPhone,Pilots2.PilotCode as SecondPilotCode,Pilots2.eNotifyAddress as SecondPilotSmsEmailAddress,
            CAST((IF(Pilots2.Filler3='Yes',1,0)) AS Bit) as SecondPilotRestEarned,
            CAST((IF(Pilots2.DataA2='Yes',1,0)) AS Bit) as SecondPilotHasTransportation,

            Pilots3.PilotName as ThirdPilotName, Pilots3.Phone1 as ThirdPilotPhone,Pilots3.PilotCode as ThirdPilotCode, Pilots3.eNotifyAddress as ThirdPilotSmsEmailAddress,
            CAST((IF(Pilots3.Filler3='Yes',1,0)) AS Bit) as ThirdPilotRestEarned,
            CAST((IF(Pilots3.DataA2='Yes',1,0)) AS Bit) as ThirdPilotHasTransportation              
              ,Apprentice1
              ,Apprentice2
              ,Apprentice3
              ,Apprentice4
              ,Estimated
              ,Confirmed
              ,DATEADD(MINUTE,MINUTE(Order_Time),    DATEADD(HOUR,HOUR (Order_Time),Est__Date_on_Board)) 
            AS OrderDateTime
           
            FROM Rundown_Table 
            LEFT JOIN Vessels ON Call_Sign=Vessels.CallSign
            LEFT JOIN Agent_Codes ON Order_Agent_Code = Agent_Codes.AgentCode
            LEFT JOIN Mileage_Codes As CodesFrom ON From_Mileage_Code = CodesFrom.MileCode
            LEFT JOIN Mileage_Codes As CodesTo ON To_Mileage_Code = CodesTo.MileCode
            LEFT JOIN Channels AS ChannelsFrom ON CodesFrom.Channel = ChannelsFrom.Channel_Code
            LEFT JOIN Channels AS ChannelsTo ON CodesTo.Channel = ChannelsTo.Channel_Code
            LEFT JOIN Pilots AS Pilots ON First_Pilot_Code = Pilots.PilotCode
            LEFT JOIN Pilots AS Pilots2 ON Second_Pilot_Code = Pilots2.PilotCode
            LEFT JOIN Pilots AS Pilots3 ON Third_Pilot_Code = Pilots3.PilotCode
            WHERE Turn_Status_Pending IN ('A') AND Direction = 'INBOUND' 
            ORDER BY Est__Date_on_Board, Order_Time, MileNumTo";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();


                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new Rundown_Inbound(reader, UsePilotCode);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        public static List<Details> VesselDetailsInbound(PsqlConnection conn, string callSign)
        {
            var intDetail = new List<Details>();

            string cmdstring = String.Format(@"SELECT * FROM Vessels WHERE CallSign = '{0}'", callSign);

            PsqlCommand comm = new PsqlCommand(cmdstring, conn);

            try
            {
                conn.Open();
                PsqlDataReader reader = comm.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        if (reader["LOAFeet"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "LOA";
                            dt.RowValue = reader["LOAFeet"].ToString().Trim();
                            intDetail.Add(dt);
                        }

                        if (reader["BredthFeet"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Beam";
                            dt.RowValue = reader["BredthFeet"].ToString().Trim();
                            intDetail.Add(dt);
                        }

                        if (reader["DWT"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "DWT";
                            dt.RowValue = Convert.ToDouble(reader["DWT"]).ToString("N0");   //            string.Format("{0,12:N0}", (double)reader["DWT"]); 
                            intDetail.Add(dt);
                        }
                        if (reader["GrossRegisteredTons"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "GRT";
                            dt.RowValue = Convert.ToDouble(reader["GrossRegisteredTons"]).ToString("N0");   //            string.Format("{0,12:N0}", (double)reader["DWT"]); 
                            intDetail.Add(dt);
                        }
                        if (reader["Ship_Status"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Status";
                            dt.RowValue = reader["Ship_Status"].ToString().Trim();
                            intDetail.Add(dt);
                        }

                        if (reader["Ship_Status_Radar"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Status Radar";
                            dt.RowValue = reader["Ship_Status_Radar"].ToString().Trim();
                            intDetail.Add(dt);
                        }
                        if (reader["Ship_Status_Radio"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Status Radio";
                            dt.RowValue = reader["Ship_Status_Radio"].ToString().Trim();
                            intDetail.Add(dt);
                        }
                        if (reader["Ship_Status_Helm"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Status Helmr";
                            dt.RowValue = reader["Ship_Status_Helm"].ToString().Trim();
                            intDetail.Add(dt);
                        }
                        if (reader["Ship_Status_Propulsi"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Status Propulsion";
                            dt.RowValue = reader["Ship_Status_Propulsi"].ToString().Trim();
                            intDetail.Add(dt);
                        }
                        if (reader["Ship_Status_Auxillia"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Status Auxiliary";
                            dt.RowValue = reader["Ship_Status_Auxilliar"].ToString().Trim();
                            intDetail.Add(dt);
                        }

                        if (reader["Vessel_Note"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Note";
                            dt.RowValue = reader["Vessel_Note"].ToString().Trim();
                            intDetail.Add(dt);
                        }

                        if (reader["HoldDescription"].ToString().Trim().Length > 0 || reader["HoldYN"].ToString().Trim() == "Y")
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Hold";
                            dt.RowValue = reader["HoldDescription"].ToString().Trim();
                            intDetail.Add(dt);
                        }

                        if (reader["Restriction"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Restriction";
                            dt.RowValue = reader["Restriction"].ToString().Trim();
                            intDetail.Add(dt);
                        }



                        if (reader["Previous_Name"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Previous Name";
                            dt.RowValue = reader["Previous_Name"].ToString().Trim();
                            intDetail.Add(dt);
                        }

                        if (reader["Flag"].ToString().Trim().Length > 0)
                        {
                            Details dt = new Details();
                            dt.RowHeader = "Flag";
                            dt.RowValue = reader["Flag"].ToString().Trim();
                            intDetail.Add(dt);
                        }
                    }
                }
            }
            catch (PsqlException pex)
            {
                //    txtError.Visible = true;
                //    txtError.Text = pex.ToString();
            }
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

            if (intDetail.Count == 0)
            {
                Details dt = new Details();
                dt.RowHeader = "";
                dt.RowValue = "No Details found";
                intDetail.Add(dt);
            }

            return intDetail;
        }

        internal static List<RundownDatabounds> sqlRundownBaseOutbounds(PsqlConnection conn, string UsePilotCode)
        {
            var intList = new List<RundownDatabounds>();

            #region cmdString
            var cmdString = @"SELECT Est__Date_on_Board as EstDateOnBoard, Time_Estimate as TimeEstimate, Call_Sign as CallSign
            , Vessel_Name as VesselName, MileNumFrom, Confirmed,
            From_Mileage_Code as FromMileageCode, MileNumTo, To_Mileage_Code as ToMileageCode, Draft_Footage as RundownDraftFeet, Draft_Inches as RundownDraftInches,
            ChangeDate, ChangeTime, LastChange, Extra_YN as ExtraYesNo, Call_Time as CallTime, Finish_Date as FinishDate, Order_Time as OrderTime,
            AirDraftFt as AirDraftFeet, AirDraftInches, Rundown_Table.Notes as RundownNotes, Order_Agent_Code as RundownOrderAgentCode,C09_Time as C09Time,
            NRI_Time as NriTime,Sun5_Time as Sun5Time, Rundown_Table.ETA as ClearTime, SBB_Time as SbbTime,C9_30_Time as C930Time,MSQPT_Time as MsqptTime,
            Assigned_Date as AssignedDate, Assigned_Time as AssignedTime, Turn_Status_Pending as TurnStatus,First_Pilot_Code as FirstPilotCode, 
            Second_Pilot_Code as SecondPilotCode, Third_Pilot_Code as ThirdPilotCode, Tug_Primary as TugPrimary,
            Extreme_Beam as RundownExtremeBeam, On_Board_Date as OnBoardDate,Second_Pilot_Finish as SecondPilotFinishTime,
            Vessels.Restriction as VesselRestriction, Vessels.DWT as VesselDwt, Vessels.LOAFeet as VesselLoa, Vessels.Type as VesselType, 
            Vessels.BredthFeet as VesselBredthFeet, Vessels.Flag as VesselFlag, Vessels.Vessel_Note as VesselNote, Vessels.HoldDescription as VesselHoldDescription,
            Vessels.LR_IMO_Ship_Number as VesselImo, Vessels.Extreme_Beam as VesselBeam,Vessels.HoldYN as VesselHoldYesNo, Vessels.Restriction_Date as VesselRestrictionDate,
            Vessels.Hold_Date as VesselHoldDate,
            Agent_Codes.AgentName, Agent_Codes.Phone as AgentPhone, Agent_Codes.Phone_1 AS AgentPhone1, Agent_Codes.Cell_1 AS AgentCell1, 
            Agent_Codes.Pager_1 AS AgentPager1, Agent_Codes.AgentCode,
            CodesFrom.Phone_Notes as CodesPhoneNotesFrom, CodesFrom.Description1 as CodesDescriptionFrom, CodesFrom.Channel as CodesChannelFrom,
            CodesTo.Phone_Notes as CodesPhoneNotesTo, CodesTo.Description1 as CodesDescriptionTo, CodesTo.Channel as CodesChannelTo,

            ChannelsFrom.Channel_Name as ChannelNameFrom, ChannelsFrom.Max_Combined_Beam as ChannelMaxBeamFrom, ChannelsFrom.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitFrom,
            ChannelsFrom.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitFrom,ChannelsFrom.Channel_Code as ChannelCodeFrom, 

            ChannelsTo.Channel_Name as ChannelNameTo, ChannelsTo.Max_Combined_Beam as ChannelMaxBeamTo, ChannelsTo.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitTo,
            ChannelsTo.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitTo,ChannelsTo.Channel_Code as ChannelCodeTo,

            Pilots.PilotName as PilotName, Pilots.Phone1 as PilotPhone, Pilots.PilotCode, Pilots.eNotifyAddress as PilotSmsEmailAddress,
            CAST((IF(Pilots.Filler3='Yes',1,0)) AS Bit) as PilotRestEarned,
            CAST((IF(Pilots.DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation,

            Pilots2.PilotName as SecondPilotName, Pilots2.Phone1 as SecondPilotPhone,Pilots2.PilotCode as SecondPilotCode,Pilots2.eNotifyAddress as SecondPilotSmsEmailAddress,
            CAST((IF(Pilots2.Filler3='Yes',1,0)) AS Bit) as SecondPilotRestEarned,
            CAST((IF(Pilots2.DataA2='Yes',1,0)) AS Bit) as SecondPilotHasTransportation,

            Pilots3.PilotName as ThirdPilotName, Pilots3.Phone1 as ThirdPilotPhone,Pilots3.PilotCode as ThirdPilotCode, Pilots3.eNotifyAddress as ThirdPilotSmsEmailAddress,
            CAST((IF(Pilots3.Filler3='Yes',1,0)) AS Bit) as ThirdPilotRestEarned,
            CAST((IF(Pilots3.DataA2='Yes',1,0)) AS Bit) as ThirdPilotHasTransportation              
              ,Apprentice1
              ,Apprentice2
              ,Apprentice3
              ,Apprentice4
              ,Estimated
              ,Confirmed
              ,DATEADD(MINUTE,MINUTE(Order_Time),    DATEADD(HOUR,HOUR (Order_Time),Est__Date_on_Board)) 
            AS OrderDateTime
                      ,Second_Pilot_Finish 

            FROM Rundown_Table 
            LEFT JOIN Vessels ON Call_Sign=Vessels.CallSign
            LEFT JOIN Agent_Codes ON Order_Agent_Code = Agent_Codes.AgentCode
            LEFT JOIN Mileage_Codes As CodesFrom ON From_Mileage_Code = CodesFrom.MileCode
            LEFT JOIN Mileage_Codes As CodesTo ON To_Mileage_Code = CodesTo.MileCode
            LEFT JOIN Channels AS ChannelsFrom ON CodesFrom.Channel = ChannelsFrom.Channel_Code
            LEFT JOIN Channels AS ChannelsTo ON CodesTo.Channel = ChannelsTo.Channel_Code
            LEFT JOIN Pilots AS Pilots ON First_Pilot_Code = Pilots.PilotCode
            LEFT JOIN Pilots AS Pilots2 ON Second_Pilot_Code = Pilots2.PilotCode
            LEFT JOIN Pilots AS Pilots3 ON Third_Pilot_Code = Pilots3.PilotCode
            WHERE Turn_Status_Pending IN ('A') AND Direction = 'OUTBOUND' 
            ORDER BY Est__Date_on_Board, Order_Time, MileNumTo";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new RundownDatabounds(reader, UsePilotCode);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        internal static List<RundownCsHarborShifts> sqlRundownBaseCsHarborShifts(PsqlConnection conn, string UsePilotCode)
        {
            var intList = new List<RundownCsHarborShifts>();

            #region cmdString
            var cmdString = @"SELECT Est__Date_on_Board as EstDateOnBoard, Time_Estimate as TimeEstimate, Call_Sign as CallSign
            , Vessel_Name as VesselName, MileNumFrom, Confirmed,
            From_Mileage_Code as FromMileageCode, MileNumTo, To_Mileage_Code as ToMileageCode, Draft_Footage as RundownDraftFeet, Draft_Inches as RundownDraftInches,
            ChangeDate, ChangeTime, LastChange, Extra_YN as ExtraYesNo, Call_Time as CallTime, Finish_Date as FinishDate, Order_Time as OrderTime,
            AirDraftFt as AirDraftFeet, AirDraftInches, Rundown_Table.Notes as RundownNotes, Order_Agent_Code as RundownOrderAgentCode,C09_Time as C09Time,
            NRI_Time as NriTime,Sun5_Time as Sun5Time, Rundown_Table.ETA as ClearTime, SBB_Time as SbbTime,C9_30_Time as C930Time,MSQPT_Time as MsqptTime,
            Assigned_Date as AssignedDate, Assigned_Time as AssignedTime, Turn_Status_Pending as TurnStatus,First_Pilot_Code as FirstPilotCode, 
            Second_Pilot_Code as SecondPilotCode, Third_Pilot_Code as ThirdPilotCode, Tug_Primary as TugPrimary,
            Extreme_Beam as RundownExtremeBeam, On_Board_Date as OnBoardDate,Second_Pilot_Finish as SecondPilotFinishTime,
            Vessels.Restriction as VesselRestriction, Vessels.DWT as VesselDwt, Vessels.LOAFeet as VesselLoa, Vessels.Type as VesselType, 
            Vessels.BredthFeet as VesselBredthFeet, Vessels.Flag as VesselFlag, Vessels.Vessel_Note as VesselNote, Vessels.HoldDescription as VesselHoldDescription,
            Vessels.LR_IMO_Ship_Number as VesselImo, Vessels.Extreme_Beam as VesselBeam,Vessels.HoldYN as VesselHoldYesNo, Vessels.Restriction_Date as VesselRestrictionDate,
            Vessels.Hold_Date as VesselHoldDate,
            Agent_Codes.AgentName, Agent_Codes.Phone as AgentPhone, Agent_Codes.Phone_1 AS AgentPhone1, Agent_Codes.Cell_1 AS AgentCell1, 
            Agent_Codes.Pager_1 AS AgentPager1, Agent_Codes.AgentCode,
            CodesFrom.Phone_Notes as CodesPhoneNotesFrom, CodesFrom.Description1 as CodesDescriptionFrom, CodesFrom.Channel as CodesChannelFrom,
            CodesTo.Phone_Notes as CodesPhoneNotesTo, CodesTo.Description1 as CodesDescriptionTo, CodesTo.Channel as CodesChannelTo,

            ChannelsFrom.Channel_Name as ChannelNameFrom, ChannelsFrom.Max_Combined_Beam as ChannelMaxBeamFrom, ChannelsFrom.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitFrom,
            ChannelsFrom.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitFrom,ChannelsFrom.Channel_Code as ChannelCodeFrom, 

            ChannelsTo.Channel_Name as ChannelNameTo, ChannelsTo.Max_Combined_Beam as ChannelMaxBeamTo, ChannelsTo.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitTo,
            ChannelsTo.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitTo,ChannelsTo.Channel_Code as ChannelCodeTo,

            Pilots.PilotName as PilotName, Pilots.Phone1 as PilotPhone, Pilots.PilotCode, Pilots.eNotifyAddress as PilotSmsEmailAddress,
            CAST((IF(Pilots.Filler3='Yes',1,0)) AS Bit) as PilotRestEarned,
            CAST((IF(Pilots.DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation,

            Pilots2.PilotName as SecondPilotName, Pilots2.Phone1 as SecondPilotPhone,Pilots2.PilotCode as SecondPilotCode,Pilots2.eNotifyAddress as SecondPilotSmsEmailAddress,
            CAST((IF(Pilots2.Filler3='Yes',1,0)) AS Bit) as SecondPilotRestEarned,
            CAST((IF(Pilots2.DataA2='Yes',1,0)) AS Bit) as SecondPilotHasTransportation,

            Pilots3.PilotName as ThirdPilotName, Pilots3.Phone1 as ThirdPilotPhone,Pilots3.PilotCode as ThirdPilotCode, Pilots3.eNotifyAddress as ThirdPilotSmsEmailAddress,
            CAST((IF(Pilots3.Filler3='Yes',1,0)) AS Bit) as ThirdPilotRestEarned,
            CAST((IF(Pilots3.DataA2='Yes',1,0)) AS Bit) as ThirdPilotHasTransportation              
              ,Apprentice1
              ,Apprentice2
              ,Apprentice3
              ,Apprentice4
              ,Estimated
              ,Confirmed
              ,DATEADD(MINUTE,MINUTE(Order_Time),    DATEADD(HOUR,HOUR (Order_Time),Est__Date_on_Board)) 
            AS OrderDateTime
           
            FROM Rundown_Table 
            LEFT JOIN Vessels ON Call_Sign=Vessels.CallSign
            LEFT JOIN Agent_Codes ON Order_Agent_Code = Agent_Codes.AgentCode
            LEFT JOIN Mileage_Codes As CodesFrom ON From_Mileage_Code = CodesFrom.MileCode
            LEFT JOIN Mileage_Codes As CodesTo ON To_Mileage_Code = CodesTo.MileCode
            LEFT JOIN Channels AS ChannelsFrom ON CodesFrom.Channel = ChannelsFrom.Channel_Code
            LEFT JOIN Channels AS ChannelsTo ON CodesTo.Channel = ChannelsTo.Channel_Code
            LEFT JOIN Pilots AS Pilots ON First_Pilot_Code = Pilots.PilotCode
            LEFT JOIN Pilots AS Pilots2 ON Second_Pilot_Code = Pilots2.PilotCode
            LEFT JOIN Pilots AS Pilots3 ON Third_Pilot_Code = Pilots3.PilotCode
            WHERE Turn_Status_Pending IN ('A') AND Direction = 'SHIFTING' 
            ORDER BY Est__Date_on_Board, Order_Time, MileNumTo";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new RundownCsHarborShifts(reader, UsePilotCode);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }


        internal static List<RundownProjectedInbounds> sqlRundownBaseProjectedInbounds(PsqlConnection conn)
        {
            var intList = new List<RundownProjectedInbounds>();

            #region cmdString
            var cmdString = @"SELECT Est__Date_on_Board as EstDateOnBoard, Time_Estimate as TimeEstimate, Call_Sign as CallSign
            , Vessel_Name as VesselName, MileNumFrom, Confirmed,
            From_Mileage_Code as FromMileageCode, MileNumTo, To_Mileage_Code as ToMileageCode, Draft_Footage as RundownDraftFeet, Draft_Inches as RundownDraftInches,
            ChangeDate, ChangeTime, LastChange, Extra_YN as ExtraYesNo, Call_Time as CallTime, Finish_Date as FinishDate, Order_Time as OrderTime,
            AirDraftFt as AirDraftFeet, AirDraftInches, Rundown_Table.Notes as RundownNotes, Order_Agent_Code as RundownOrderAgentCode,C09_Time as C09Time,
            NRI_Time as NriTime,Sun5_Time as Sun5Time, Rundown_Table.ETA as ClearTime, SBB_Time as SbbTime,C9_30_Time as C930Time,MSQPT_Time as MsqptTime,
            Assigned_Date as AssignedDate, Assigned_Time as AssignedTime, Turn_Status_Pending as TurnStatus,First_Pilot_Code as FirstPilotCode, 
            Second_Pilot_Code as SecondPilotCode, Third_Pilot_Code as ThirdPilotCode, Tug_Primary as TugPrimary,
            Extreme_Beam as RundownExtremeBeam, On_Board_Date as OnBoardDate,Second_Pilot_Finish as SecondPilotFinishTime,
            Vessels.Restriction as VesselRestriction, Vessels.DWT as VesselDwt, Vessels.LOAFeet as VesselLoa, Vessels.Type as VesselType, 
            Vessels.BredthFeet as VesselBredthFeet, Vessels.Flag as VesselFlag, Vessels.Vessel_Note as VesselNote, Vessels.HoldDescription as VesselHoldDescription,
            Vessels.LR_IMO_Ship_Number as VesselImo, Vessels.Extreme_Beam as VesselBeam,Vessels.HoldYN as VesselHoldYesNo, Vessels.Restriction_Date as VesselRestrictionDate,
            Vessels.Hold_Date as VesselHoldDate,
            Agent_Codes.AgentName, Agent_Codes.Phone as AgentPhone, Agent_Codes.Phone_1 AS AgentPhone1, Agent_Codes.Cell_1 AS AgentCell1, 
            Agent_Codes.Pager_1 AS AgentPager1, Agent_Codes.AgentCode,
            CodesFrom.Phone_Notes as CodesPhoneNotesFrom, CodesFrom.Description1 as CodesDescriptionFrom, CodesFrom.Channel as CodesChannelFrom,
            CodesTo.Phone_Notes as CodesPhoneNotesTo, CodesTo.Description1 as CodesDescriptionTo, CodesTo.Channel as CodesChannelTo,

            ChannelsFrom.Channel_Name as ChannelNameFrom, ChannelsFrom.Max_Combined_Beam as ChannelMaxBeamFrom, ChannelsFrom.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitFrom,
            ChannelsFrom.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitFrom,ChannelsFrom.Channel_Code as ChannelCodeFrom, 

            ChannelsTo.Channel_Name as ChannelNameTo, ChannelsTo.Max_Combined_Beam as ChannelMaxBeamTo, ChannelsTo.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitTo,
            ChannelsTo.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitTo,ChannelsTo.Channel_Code as ChannelCodeTo,

            Pilots.PilotName as PilotName, Pilots.Phone1 as PilotPhone, Pilots.PilotCode, Pilots.eNotifyAddress as PilotSmsEmailAddress,
            CAST((IF(Pilots.Filler3='Yes',1,0)) AS Bit) as PilotRestEarned,
            CAST((IF(Pilots.DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation,

            Pilots2.PilotName as SecondPilotName, Pilots2.Phone1 as SecondPilotPhone,Pilots2.PilotCode as SecondPilotCode,Pilots2.eNotifyAddress as SecondPilotSmsEmailAddress,
            CAST((IF(Pilots2.Filler3='Yes',1,0)) AS Bit) as SecondPilotRestEarned,
            CAST((IF(Pilots2.DataA2='Yes',1,0)) AS Bit) as SecondPilotHasTransportation,

            Pilots3.PilotName as ThirdPilotName, Pilots3.Phone1 as ThirdPilotPhone,Pilots3.PilotCode as ThirdPilotCode, Pilots3.eNotifyAddress as ThirdPilotSmsEmailAddress,
            CAST((IF(Pilots3.Filler3='Yes',1,0)) AS Bit) as ThirdPilotRestEarned,
            CAST((IF(Pilots3.DataA2='Yes',1,0)) AS Bit) as ThirdPilotHasTransportation              
              ,Apprentice1
              ,Apprentice2
              ,Apprentice3
              ,Apprentice4
              ,Estimated
              ,Confirmed
              ,DATEADD(MINUTE,MINUTE(Order_Time),    DATEADD(HOUR,HOUR (Order_Time),Est__Date_on_Board)) 
            AS OrderDateTime
           
            FROM Rundown_Table 
            LEFT JOIN Vessels ON Call_Sign=Vessels.CallSign
            LEFT JOIN Agent_Codes ON Order_Agent_Code = Agent_Codes.AgentCode
            LEFT JOIN Mileage_Codes As CodesFrom ON From_Mileage_Code = CodesFrom.MileCode
            LEFT JOIN Mileage_Codes As CodesTo ON To_Mileage_Code = CodesTo.MileCode
            LEFT JOIN Channels AS ChannelsFrom ON CodesFrom.Channel = ChannelsFrom.Channel_Code
            LEFT JOIN Channels AS ChannelsTo ON CodesTo.Channel = ChannelsTo.Channel_Code
            LEFT JOIN Pilots AS Pilots ON First_Pilot_Code = Pilots.PilotCode
            LEFT JOIN Pilots AS Pilots2 ON Second_Pilot_Code = Pilots2.PilotCode
            LEFT JOIN Pilots AS Pilots3 ON Third_Pilot_Code = Pilots3.PilotCode
            WHERE Turn_Status_Pending IN ('O') AND Direction = 'INBOUND' 
            ORDER BY Est__Date_on_Board, Order_Time, MileNumTo";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new RundownProjectedInbounds(reader);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        internal static List<RundownProjectedOutbounds> sqlRundownBaseProjectedOutbounds(PsqlConnection conn)
        {
            var intList = new List<RundownProjectedOutbounds>();

            #region cmdString
            var cmdString = @"SELECT Est__Date_on_Board as EstDateOnBoard, Time_Estimate as TimeEstimate, Call_Sign as CallSign
            , Vessel_Name as VesselName, MileNumFrom, Confirmed,
            From_Mileage_Code as FromMileageCode, MileNumTo, To_Mileage_Code as ToMileageCode, Draft_Footage as RundownDraftFeet, Draft_Inches as RundownDraftInches,
            ChangeDate, ChangeTime, LastChange, Extra_YN as ExtraYesNo, Call_Time as CallTime, Finish_Date as FinishDate, Order_Time as OrderTime,
            AirDraftFt as AirDraftFeet, AirDraftInches, Rundown_Table.Notes as RundownNotes, Order_Agent_Code as RundownOrderAgentCode,C09_Time as C09Time,
            NRI_Time as NriTime,Sun5_Time as Sun5Time, Rundown_Table.ETA as ClearTime, SBB_Time as SbbTime,C9_30_Time as C930Time,MSQPT_Time as MsqptTime,
            Assigned_Date as AssignedDate, Assigned_Time as AssignedTime, Turn_Status_Pending as TurnStatus,First_Pilot_Code as FirstPilotCode, 
            Second_Pilot_Code as SecondPilotCode, Third_Pilot_Code as ThirdPilotCode, Tug_Primary as TugPrimary,
            Extreme_Beam as RundownExtremeBeam, On_Board_Date as OnBoardDate,Second_Pilot_Finish as SecondPilotFinishTime,
            Vessels.Restriction as VesselRestriction, Vessels.DWT as VesselDwt, Vessels.LOAFeet as VesselLoa, Vessels.Type as VesselType, 
            Vessels.BredthFeet as VesselBredthFeet, Vessels.Flag as VesselFlag, Vessels.Vessel_Note as VesselNote, Vessels.HoldDescription as VesselHoldDescription,
            Vessels.LR_IMO_Ship_Number as VesselImo, Vessels.Extreme_Beam as VesselBeam,Vessels.HoldYN as VesselHoldYesNo, Vessels.Restriction_Date as VesselRestrictionDate,
            Vessels.Hold_Date as VesselHoldDate,
            Agent_Codes.AgentName, Agent_Codes.Phone as AgentPhone, Agent_Codes.Phone_1 AS AgentPhone1, Agent_Codes.Cell_1 AS AgentCell1, 
            Agent_Codes.Pager_1 AS AgentPager1, Agent_Codes.AgentCode,
            CodesFrom.Phone_Notes as CodesPhoneNotesFrom, CodesFrom.Description1 as CodesDescriptionFrom, CodesFrom.Channel as CodesChannelFrom,
            CodesTo.Phone_Notes as CodesPhoneNotesTo, CodesTo.Description1 as CodesDescriptionTo, CodesTo.Channel as CodesChannelTo,

            ChannelsFrom.Channel_Name as ChannelNameFrom, ChannelsFrom.Max_Combined_Beam as ChannelMaxBeamFrom, ChannelsFrom.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitFrom,
            ChannelsFrom.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitFrom,ChannelsFrom.Channel_Code as ChannelCodeFrom, 

            ChannelsTo.Channel_Name as ChannelNameTo, ChannelsTo.Max_Combined_Beam as ChannelMaxBeamTo, ChannelsTo.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitTo,
            ChannelsTo.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitTo,ChannelsTo.Channel_Code as ChannelCodeTo,

            Pilots.PilotName as PilotName, Pilots.Phone1 as PilotPhone, Pilots.PilotCode, Pilots.eNotifyAddress as PilotSmsEmailAddress,
            CAST((IF(Pilots.Filler3='Yes',1,0)) AS Bit) as PilotRestEarned,
            CAST((IF(Pilots.DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation,

            Pilots2.PilotName as SecondPilotName, Pilots2.Phone1 as SecondPilotPhone,Pilots2.PilotCode as SecondPilotCode,Pilots2.eNotifyAddress as SecondPilotSmsEmailAddress,
            CAST((IF(Pilots2.Filler3='Yes',1,0)) AS Bit) as SecondPilotRestEarned,
            CAST((IF(Pilots2.DataA2='Yes',1,0)) AS Bit) as SecondPilotHasTransportation,

            Pilots3.PilotName as ThirdPilotName, Pilots3.Phone1 as ThirdPilotPhone,Pilots3.PilotCode as ThirdPilotCode, Pilots3.eNotifyAddress as ThirdPilotSmsEmailAddress,
            CAST((IF(Pilots3.Filler3='Yes',1,0)) AS Bit) as ThirdPilotRestEarned,
            CAST((IF(Pilots3.DataA2='Yes',1,0)) AS Bit) as ThirdPilotHasTransportation              
              ,Apprentice1
              ,Apprentice2
              ,Apprentice3
              ,Apprentice4
              ,Estimated
              ,Confirmed
              ,DATEADD(MINUTE,MINUTE(Order_Time),    DATEADD(HOUR,HOUR (Order_Time),Est__Date_on_Board)) 
            AS OrderDateTime
           
            FROM Rundown_Table 
            LEFT JOIN Vessels ON Call_Sign=Vessels.CallSign
            LEFT JOIN Agent_Codes ON Order_Agent_Code = Agent_Codes.AgentCode
            LEFT JOIN Mileage_Codes As CodesFrom ON From_Mileage_Code = CodesFrom.MileCode
            LEFT JOIN Mileage_Codes As CodesTo ON To_Mileage_Code = CodesTo.MileCode
            LEFT JOIN Channels AS ChannelsFrom ON CodesFrom.Channel = ChannelsFrom.Channel_Code
            LEFT JOIN Channels AS ChannelsTo ON CodesTo.Channel = ChannelsTo.Channel_Code
            LEFT JOIN Pilots AS Pilots ON First_Pilot_Code = Pilots.PilotCode
            LEFT JOIN Pilots AS Pilots2 ON Second_Pilot_Code = Pilots2.PilotCode
            LEFT JOIN Pilots AS Pilots3 ON Third_Pilot_Code = Pilots3.PilotCode
            WHERE Turn_Status_Pending IN ('O') AND Direction = 'OUTBOUND' 
            ORDER BY Est__Date_on_Board, Order_Time, MileNumTo";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new RundownProjectedOutbounds(reader);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        internal static List<RundownProjectedShifts> sqlRundownBaseProjectedShifts(PsqlConnection conn)
        {
            var intList = new List<RundownProjectedShifts>();

            #region cmdString
            var cmdString = @"SELECT Est__Date_on_Board as EstDateOnBoard, Time_Estimate as TimeEstimate, Call_Sign as CallSign
            , Vessel_Name as VesselName, MileNumFrom, Confirmed,
            From_Mileage_Code as FromMileageCode, MileNumTo, To_Mileage_Code as ToMileageCode, Draft_Footage as RundownDraftFeet, Draft_Inches as RundownDraftInches,
            ChangeDate, ChangeTime, LastChange, Extra_YN as ExtraYesNo, Call_Time as CallTime, Finish_Date as FinishDate, Order_Time as OrderTime,
            AirDraftFt as AirDraftFeet, AirDraftInches, Rundown_Table.Notes as RundownNotes, Order_Agent_Code as RundownOrderAgentCode,C09_Time as C09Time,
            NRI_Time as NriTime,Sun5_Time as Sun5Time, Rundown_Table.ETA as ClearTime, SBB_Time as SbbTime,C9_30_Time as C930Time,MSQPT_Time as MsqptTime,
            Assigned_Date as AssignedDate, Assigned_Time as AssignedTime, Turn_Status_Pending as TurnStatus,First_Pilot_Code as FirstPilotCode, 
            Second_Pilot_Code as SecondPilotCode, Third_Pilot_Code as ThirdPilotCode, Tug_Primary as TugPrimary,
            Extreme_Beam as RundownExtremeBeam, On_Board_Date as OnBoardDate,Second_Pilot_Finish as SecondPilotFinishTime,
            Vessels.Restriction as VesselRestriction, Vessels.DWT as VesselDwt, Vessels.LOAFeet as VesselLoa, Vessels.Type as VesselType, 
            Vessels.BredthFeet as VesselBredthFeet, Vessels.Flag as VesselFlag, Vessels.Vessel_Note as VesselNote, Vessels.HoldDescription as VesselHoldDescription,
            Vessels.LR_IMO_Ship_Number as VesselImo, Vessels.Extreme_Beam as VesselBeam,Vessels.HoldYN as VesselHoldYesNo, Vessels.Restriction_Date as VesselRestrictionDate,
            Vessels.Hold_Date as VesselHoldDate,
            Agent_Codes.AgentName, Agent_Codes.Phone as AgentPhone, Agent_Codes.Phone_1 AS AgentPhone1, Agent_Codes.Cell_1 AS AgentCell1, 
            Agent_Codes.Pager_1 AS AgentPager1, Agent_Codes.AgentCode,
            CodesFrom.Phone_Notes as CodesPhoneNotesFrom, CodesFrom.Description1 as CodesDescriptionFrom, CodesFrom.Channel as CodesChannelFrom,
            CodesTo.Phone_Notes as CodesPhoneNotesTo, CodesTo.Description1 as CodesDescriptionTo, CodesTo.Channel as CodesChannelTo,

            ChannelsFrom.Channel_Name as ChannelNameFrom, ChannelsFrom.Max_Combined_Beam as ChannelMaxBeamFrom, ChannelsFrom.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitFrom,
            ChannelsFrom.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitFrom,ChannelsFrom.Channel_Code as ChannelCodeFrom, 

            ChannelsTo.Channel_Name as ChannelNameTo, ChannelsTo.Max_Combined_Beam as ChannelMaxBeamTo, ChannelsTo.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitTo,
            ChannelsTo.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitTo,ChannelsTo.Channel_Code as ChannelCodeTo,

            Pilots.PilotName as PilotName, Pilots.Phone1 as PilotPhone, Pilots.PilotCode, Pilots.eNotifyAddress as PilotSmsEmailAddress,
            CAST((IF(Pilots.Filler3='Yes',1,0)) AS Bit) as PilotRestEarned,
            CAST((IF(Pilots.DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation,

            Pilots2.PilotName as SecondPilotName, Pilots2.Phone1 as SecondPilotPhone,Pilots2.PilotCode as SecondPilotCode,Pilots2.eNotifyAddress as SecondPilotSmsEmailAddress,
            CAST((IF(Pilots2.Filler3='Yes',1,0)) AS Bit) as SecondPilotRestEarned,
            CAST((IF(Pilots2.DataA2='Yes',1,0)) AS Bit) as SecondPilotHasTransportation,

            Pilots3.PilotName as ThirdPilotName, Pilots3.Phone1 as ThirdPilotPhone,Pilots3.PilotCode as ThirdPilotCode, Pilots3.eNotifyAddress as ThirdPilotSmsEmailAddress,
            CAST((IF(Pilots3.Filler3='Yes',1,0)) AS Bit) as ThirdPilotRestEarned,
            CAST((IF(Pilots3.DataA2='Yes',1,0)) AS Bit) as ThirdPilotHasTransportation              
              ,Apprentice1
              ,Apprentice2
              ,Apprentice3
              ,Apprentice4
              ,Estimated
              ,Confirmed
              ,DATEADD(MINUTE,MINUTE(Order_Time),    DATEADD(HOUR,HOUR (Order_Time),Est__Date_on_Board)) 
            AS OrderDateTime
            FROM Rundown_Table 
            LEFT JOIN Vessels ON Call_Sign=Vessels.CallSign
            LEFT JOIN Agent_Codes ON Order_Agent_Code = Agent_Codes.AgentCode
            LEFT JOIN Mileage_Codes As CodesFrom ON From_Mileage_Code = CodesFrom.MileCode
            LEFT JOIN Mileage_Codes As CodesTo ON To_Mileage_Code = CodesTo.MileCode
            LEFT JOIN Channels AS ChannelsFrom ON CodesFrom.Channel = ChannelsFrom.Channel_Code
            LEFT JOIN Channels AS ChannelsTo ON CodesTo.Channel = ChannelsTo.Channel_Code
            LEFT JOIN Pilots AS Pilots ON First_Pilot_Code = Pilots.PilotCode
            LEFT JOIN Pilots AS Pilots2 ON Second_Pilot_Code = Pilots2.PilotCode
            LEFT JOIN Pilots AS Pilots3 ON Third_Pilot_Code = Pilots3.PilotCode
            WHERE Turn_Status_Pending IN ('O') AND Direction = 'SHIFTING' 
            ORDER BY Est__Date_on_Board, Order_Time, MileNumTo";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new RundownProjectedShifts(reader);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        internal static List<Note_Kendo> sqlNoteBase(PsqlConnection conn, string ShowNotes, string ShowSeaReport, string ShowClosures)
        {
            var intList = new List<Note_Kendo>();

            #region cmdString
            var cmdString = cmdNoteBaseString(ShowNotes, ShowSeaReport, ShowClosures);
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new Note_Kendo(reader);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }


        internal static List<Alert> sqlAlertBase(PsqlConnection conn, string ShowLadder, string ShowNotices)
        {
            var intList = new List<Alert>();

            #region cmdString
            var cmdString = cmdAlertBaseString(ShowLadder, ShowNotices);
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new Alert(reader);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        internal static List<Pilot_Kendo> sqlWorkingPilotsBase(PsqlConnection conn)
        {
            Pilot_Kendo.myIndex = 1;
            Pilot_Kendo.myWorkingIndex = 1;

            var intList = new List<Pilot_Kendo>();

            #region cmdString
            var cmdString = @"SELECT Pilots.PilotCode, PilotName, LastFinishDate, LastFinishTime, AssignedYN as AssignedYesNo,Status,Pilots.GroupAB as PilotGroup,
            RestrictedYN as RestrictedYesNo,RestrictedDWT as RestrictedDwt,RestrictedDraft, RestPeriodDate, RestPeriodTime, Hire_ExtraYN as HireExtraYesNo,
            DataN2, MoneyManType1, MoneyManType2, MoneyManType3, MoneyManType4, eNotifyAddress as SmsEmailAddress,
            RestPeriodDeclineYN as RestPeriodDeclineYesNo
,HOUR (RestPeriodTime) AS restHours,MINUTE (RestPeriodTime) as restMinutes 
            ,DATEADD(HOUR,HOUR (RestPeriodTime),LastFinishDate) AS LastFinishDateTeime
            , Hire_ExtraYN as HireExtraYesNo,
            DataN2, MoneyManType1, MoneyManType2, MoneyManType3, MoneyManType4, eNotifyAddress as SmsEmailAddress,
            RestPeriodDeclineYN as RestPeriodDeclineYesNo
            ,DATEADD(MINUTE,MINUTE(LastFinishTime),    DATEADD(HOUR,HOUR (LastFinishTime),LastFinishDate)) 
  AS LastFinishDateTime
            ,DATEADD(MINUTE,MINUTE(RestPeriodTime ),    DATEADD(HOUR,HOUR (RestPeriodTime ),RestPeriodDate)) 
  AS RestPeriodStartDateTime
   ,DATEADD(HOUR,9,DATEADD(MINUTE,MINUTE(RestPeriodTime ),    DATEADD(HOUR,HOUR (RestPeriodTime ),RestPeriodDate))) 
  AS RestPeriodEndDateTime
            ,Status_Description
,  CAST((IF(Filler3='Yes',1,0)) AS Bit) as PilotRestEarned
  ,          CAST((IF(DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation
            FROM Pilots
            JOIN Status_Codes ON Pilots.Status = Status_Codes.Status_Code
            
            WHERE Status = 'W'   AND AssignedYN = 'N' 
            ORDER BY LastFinishDate, LastFinishTime  ";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new Pilot_Kendo(reader);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        internal static List<Pilot_Watch_Slate> sql_Watch_Slate_PilotsBase(PsqlConnection conn)
        {
            Pilot_Watch_Slate.myIndex = 1;

            var intList = new List<Pilot_Watch_Slate>();

            #region cmdString
            var cmdString = @"SELECT Pilots.PilotCode, PilotName, LastFinishDate, LastFinishTime, AssignedYN as AssignedYesNo,Status,Pilots.GroupAB as PilotGroup,
            RestrictedYN as RestrictedYesNo,RestrictedDWT as RestrictedDwt,RestrictedDraft, RestPeriodDate, RestPeriodTime, Hire_ExtraYN as HireExtraYesNo,
            DataN2, MoneyManType1, MoneyManType2, MoneyManType3, MoneyManType4, eNotifyAddress as SmsEmailAddress,
            RestPeriodDeclineYN as RestPeriodDeclineYesNo
,HOUR (RestPeriodTime) AS restHours,MINUTE (RestPeriodTime) as restMinutes 
            ,DATEADD(HOUR,HOUR (RestPeriodTime),LastFinishDate) AS LastFinishDateTeime
            , Hire_ExtraYN as HireExtraYesNo,
            DataN2, MoneyManType1, MoneyManType2, MoneyManType3, MoneyManType4, eNotifyAddress as SmsEmailAddress,
            RestPeriodDeclineYN as RestPeriodDeclineYesNo
            ,DATEADD(MINUTE,MINUTE(LastFinishTime),    DATEADD(HOUR,HOUR (LastFinishTime),LastFinishDate)) 
  AS LastFinishDateTime
            ,DATEADD(MINUTE,MINUTE(RestPeriodTime ),    DATEADD(HOUR,HOUR (RestPeriodTime ),RestPeriodDate)) 
  AS RestPeriodStartDateTime
   ,DATEADD(HOUR,9,DATEADD(MINUTE,MINUTE(RestPeriodTime ),    DATEADD(HOUR,HOUR (RestPeriodTime ),RestPeriodDate))) 
  AS RestPeriodEndDateTime
            ,Status_Description
  ,DataA1 As ReturningOrder
,  CAST((IF(Filler3='Yes',1,0)) AS Bit) as PilotRestEarned
  ,          CAST((IF(DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation
            FROM Pilots
            JOIN Status_Codes ON Pilots.Status = Status_Codes.Status_Code
             WHERE Status = 'OFF'    -- AND AssignedYN = 'N'
            AND PilotName NOT LIKE '%TEST%' 
            AND PilotCode <> 0.0
            ORDER BY  Datan2";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new Pilot_Watch_Slate(reader);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        internal static List<Pilot_Unavailable> sql_Pilot_UnavailablesBase(PsqlConnection conn)
        {
            Pilot_Unavailable.myIndex = 1;

            var intList = new List<Pilot_Unavailable>();

            #region cmdString
            var cmdString = @"SELECT Pilots.PilotCode, PilotName, LastFinishDate, LastFinishTime, AssignedYN as AssignedYesNo,Status,Pilots.GroupAB as PilotGroup,
            RestrictedYN as RestrictedYesNo,RestrictedDWT as RestrictedDwt,RestrictedDraft, RestPeriodDate, RestPeriodTime, Hire_ExtraYN as HireExtraYesNo,
            DataN2, MoneyManType1, MoneyManType2, MoneyManType3, MoneyManType4, eNotifyAddress as SmsEmailAddress,
            RestPeriodDeclineYN as RestPeriodDeclineYesNo
            ,HOUR (RestPeriodTime) AS restHours,MINUTE (RestPeriodTime) as restMinutes 
            ,DATEADD(HOUR,HOUR (RestPeriodTime),LastFinishDate) AS LastFinishDateTeime
            , Hire_ExtraYN as HireExtraYesNo,
            DataN2, MoneyManType1, MoneyManType2, MoneyManType3, MoneyManType4, eNotifyAddress as SmsEmailAddress,
            RestPeriodDeclineYN as RestPeriodDeclineYesNo
            ,DATEADD(MINUTE,MINUTE(LastFinishTime),    DATEADD(HOUR,HOUR (LastFinishTime),LastFinishDate)) 
             AS LastFinishDateTime
            ,DATEADD(MINUTE,MINUTE(RestPeriodTime ),    DATEADD(HOUR,HOUR (RestPeriodTime ),RestPeriodDate)) 
             AS RestPeriodStartDateTime
             ,DATEADD(HOUR,9,DATEADD(MINUTE,MINUTE(RestPeriodTime ),    DATEADD(HOUR,HOUR (RestPeriodTime ),RestPeriodDate))) 
                AS RestPeriodEndDateTime
            ,Status_Description
            ,  CAST((IF(Filler3='Yes',1,0)) AS Bit) as PilotRestEarned
             ,  CAST((IF(DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation
            FROM Pilots
            JOIN Status_Codes ON Pilots.Status = Status_Codes.Status_Code
            
              WHERE Status NOT IN ('W','OFF','RET') AND PilotName NOT LIKE 'TEST%'
            ORDER BY LastFinishDate, LastFinishTime";
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new Pilot_Unavailable(reader);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }

        private static string cmdNoteBaseString(string ShowNotes, string ShowSeaReport, string ShowClosures)
        {
            bool showNote = false;
            bool showSeaReport = false;
            bool showClosures = false;

            string strWHERE = @"WHERE Priority >= 0 ";

            if (ShowNotes.ToLower().ToString() == "true")
            {
                showNote = true;
                //whereNotes = " OR Note_type = 'NOTES' ";
            }

            if (ShowSeaReport.ToLower().ToString() == "true")
            {
                showSeaReport = true;
                //whereSeaReport = " OR Note_type = 'SEA REPORT' ";
            }


            if (ShowClosures.ToLower().ToString() == "true")
            {
                showClosures = true;
                //whereClosures = " OR Note_type LIKE 'CLOSED%'  ";
            }

            if (showNote && showSeaReport && showClosures)
            {
                strWHERE += " AND (Note_type IN ('NOTES','SEA REPORT') OR Note_type LIKE 'CLOSED%') ";
            }
            else
            {
                //Notes and other two options
                //Only Notes
                if (showNote && !showClosures && !showSeaReport)
                {
                    strWHERE += " AND Note_type = 'NOTES' ";
                }

                //Notes and Closures
                if (showNote && showClosures && !showSeaReport)
                {
                    strWHERE += " AND ( Note_type = 'NOTES' OR Note_type LIKE 'CLOSED%' ) ";
                }

                //Notes and Sea Report
                if (showNote && !showClosures && showSeaReport)
                {
                    strWHERE += " AND Note_type IN ('NOTES','SEA REPORT')  ";
                }

                //Sea report an other options
                //Sea report  only
                if (showSeaReport && !showClosures && !showNote)
                {
                    strWHERE += " AND Note_type = 'SEA REPORT' ";
                }

                //Sea report and Closures
                if (showSeaReport && showClosures && !showNote)
                {
                    strWHERE += " AND ( Note_type = 'SEA REPORT' OR Note_type LIKE 'CLOSED%' ) ";
                }
                //Sea report and Notes  (Already Done)

                // Closures and other options
                //Closure only
                if (showClosures && !showNote && !showSeaReport)
                {
                    strWHERE += " AND Note_type LIKE 'CLOSED%' ";
                }

                //Closures and Notes  (already done)

                //Closures and Sea report (already done)

            }

            string intString = string.Format(@"SELECT Notes.Priority, Note_Type as NoteType, Note1, Notes.Date as LastChangeDate, Last_Change_By as LastChange FROM Notes {0} ORDER BY Priority  ", strWHERE);
            //string intString = string.Format(@"SELECT Notes.Priority, Note_Type as NoteType, Note1, Notes.Date as LastChangeDate, Last_Change_By as LastChange FROM Notes {0} {1} {2} {3}  "
            // , strWHERE,whereNotes,whereSeaReport,whereClosures);

            return intString;

        }

        private static string cmdAlertBaseString(string ShowLadder, string ShowNotices)
        {
            bool showLadder = false;
            bool showNotice = false;

            string strWHERE = @"WHERE Priority >= 0 ";

            if (ShowLadder.ToLower() == "true")
            {

                showLadder = true;
            }


            if (ShowNotices.ToLower() == "true")
            {
                showNotice = true;

            }
            if (showLadder && showNotice)
            {
                strWHERE += " AND Note_Type IN ('Ladder','Notice') ";
            }
            else
            {
                if (showLadder)
                {
                    strWHERE += " AND Note_Type = 'Ladder' ";
                }
                if (showNotice)
                {
                    strWHERE += " AND Note_Type = 'Notice' ";
                }
            }



            string intString = string.Format(@"SELECT Alerts.Priority, Note_Type as NoteType, Note1, Alerts.Date as LastChangeDate, Last_Change_By as LastChange FROM Alerts {0}  ", strWHERE);

            return intString;

        }

        internal static List<Rundown_Docked> sql_Rundown_Dockeds(PsqlConnection conn, string DaysBack)
        {
            Rundown_Docked.myOldestDate = DateTime.Today.AddDays(-10).ToString("yyyy-MM-dd");

            var intList = new List<Rundown_Docked>();

            string intString = Rundown_Docked.myOldestDate;          
            
            #region cmdString

            string cmdString = String.Format(@"  SELECT Est__Date_on_Board as EstDateOnBoard, Time_Estimate as TimeEstimate, Call_Sign as CallSign
            , Vessel_Name as VesselName, MileNumFrom, Confirmed,
            From_Mileage_Code as FromMileageCode, MileNumTo, To_Mileage_Code as ToMileageCode, Draft_Footage as RundownDraftFeet, Draft_Inches as RundownDraftInches,
            ChangeDate, ChangeTime, LastChange, Extra_YN as ExtraYesNo, Call_Time as CallTime, Finish_Date as FinishDate, Order_Time as OrderTime,
            AirDraftFt as AirDraftFeet, AirDraftInches, Rundown_Table.Notes as RundownNotes, Order_Agent_Code as RundownOrderAgentCode,C09_Time as C09Time,
            NRI_Time as NriTime,Sun5_Time as Sun5Time, Rundown_Table.ETA as ClearTime, SBB_Time as SbbTime,C9_30_Time as C930Time,MSQPT_Time as MsqptTime,
            Assigned_Date as AssignedDate, Assigned_Time as AssignedTime, Turn_Status_Pending as TurnStatus,First_Pilot_Code as FirstPilotCode, 
            Second_Pilot_Code as SecondPilotCode, Third_Pilot_Code as ThirdPilotCode, Tug_Primary as TugPrimary,
            Extreme_Beam as RundownExtremeBeam, On_Board_Date as OnBoardDate,Second_Pilot_Finish as SecondPilotFinishTime,
            Vessels.Restriction as VesselRestriction, Vessels.DWT as VesselDwt, Vessels.LOAFeet as VesselLoa, Vessels.Type as VesselType, 
            Vessels.BredthFeet as VesselBredthFeet, Vessels.Flag as VesselFlag, Vessels.Vessel_Note as VesselNote, Vessels.HoldDescription as VesselHoldDescription,
            Vessels.LR_IMO_Ship_Number as VesselImo, Vessels.Extreme_Beam as VesselBeam,Vessels.HoldYN as VesselHoldYesNo, Vessels.Restriction_Date as VesselRestrictionDate,
            Vessels.Hold_Date as VesselHoldDate,
            Agent_Codes.AgentName, Agent_Codes.Phone as AgentPhone, Agent_Codes.Phone_1 AS AgentPhone1, Agent_Codes.Cell_1 AS AgentCell1, 
            Agent_Codes.Pager_1 AS AgentPager1, Agent_Codes.AgentCode,
            CodesFrom.Phone_Notes as CodesPhoneNotesFrom, CodesFrom.Description1 as CodesDescriptionFrom, CodesFrom.Channel as CodesChannelFrom,
            CodesTo.Phone_Notes as CodesPhoneNotesTo, CodesTo.Description1 as CodesDescriptionTo, CodesTo.Channel as CodesChannelTo,

            ChannelsFrom.Channel_Name as ChannelNameFrom, ChannelsFrom.Max_Combined_Beam as ChannelMaxBeamFrom, ChannelsFrom.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitFrom,
            ChannelsFrom.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitFrom,ChannelsFrom.Channel_Code as ChannelCodeFrom, 

            ChannelsTo.Channel_Name as ChannelNameTo, ChannelsTo.Max_Combined_Beam as ChannelMaxBeamTo, ChannelsTo.First_Vessel_DWT_Lim as ChannelFirstVesselDwtLimitTo,
            ChannelsTo.Second_Vessel_DWT_Li as ChannelSecondVesselDwtLimitTo,ChannelsTo.Channel_Code as ChannelCodeTo,

            Pilots.PilotName as PilotName, Pilots.Phone1 as PilotPhone, Pilots.PilotCode, Pilots.eNotifyAddress as PilotSmsEmailAddress,
            CAST((IF(Pilots.Filler3='Yes',1,0)) AS Bit) as PilotRestEarned,
            CAST((IF(Pilots.DataA2='Yes',1,0)) AS Bit) as PilotHasTransportation,

            Pilots2.PilotName as SecondPilotName, Pilots2.Phone1 as SecondPilotPhone,Pilots2.PilotCode as SecondPilotCode,Pilots2.eNotifyAddress as SecondPilotSmsEmailAddress,
            CAST((IF(Pilots2.Filler3='Yes',1,0)) AS Bit) as SecondPilotRestEarned,
            CAST((IF(Pilots2.DataA2='Yes',1,0)) AS Bit) as SecondPilotHasTransportation,

            Pilots3.PilotName as ThirdPilotName, Pilots3.Phone1 as ThirdPilotPhone,Pilots3.PilotCode as ThirdPilotCode, Pilots3.eNotifyAddress as ThirdPilotSmsEmailAddress,
            CAST((IF(Pilots3.Filler3='Yes',1,0)) AS Bit) as ThirdPilotRestEarned,
            CAST((IF(Pilots3.DataA2='Yes',1,0)) AS Bit) as ThirdPilotHasTransportation              
              ,Apprentice1
              ,Apprentice2
              ,Apprentice3
              ,Apprentice4
              ,Estimated
              ,Confirmed
              ,DATEADD(MINUTE,MINUTE(Order_Time),    DATEADD(HOUR,HOUR (Order_Time),Est__Date_on_Board)) 
            AS OrderDateTime
           
            FROM Rundown_Table 
            LEFT JOIN Vessels ON Call_Sign=Vessels.CallSign
            LEFT JOIN Agent_Codes ON Order_Agent_Code = Agent_Codes.AgentCode
            LEFT JOIN Mileage_Codes As CodesFrom ON From_Mileage_Code = CodesFrom.MileCode
            LEFT JOIN Mileage_Codes As CodesTo ON To_Mileage_Code = CodesTo.MileCode
            LEFT JOIN Channels AS ChannelsFrom ON CodesFrom.Channel = ChannelsFrom.Channel_Code
            LEFT JOIN Channels AS ChannelsTo ON CodesTo.Channel = ChannelsTo.Channel_Code
            LEFT JOIN Pilots AS Pilots ON First_Pilot_Code = Pilots.PilotCode
            LEFT JOIN Pilots AS Pilots2 ON Second_Pilot_Code = Pilots2.PilotCode
            LEFT JOIN Pilots AS Pilots3 ON Third_Pilot_Code = Pilots3.PilotCode
            WHERE Turn_Status_Pending IN ('Z')  AND Est__Date_on_Board >= '{0}'
            ORDER BY Est__Date_on_Board, Order_Time, MileNumTo", intString);
            #endregion

            try
            {
                var cmd = new PsqlCommand(cmdString, conn);

                cmd.Connection.Open();

                PsqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var n = new Rundown_Docked(reader, DaysBack);

                        intList.Add(n);
                    }
                }
            }
            catch (Exception exc)
            {
                var dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                             string.Format("Error on Process {0}", exc));

                Exceptions.LogException(dex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return intList;
        }



    }
}

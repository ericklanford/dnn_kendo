﻿using Generator;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TestAspNetCoreWebApp.Models;

namespace TestAspNetCoreWebApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {

            var projectPath = @"C:\Dnn_Kendo_Projects\alwaysUpdateDnnMod3\alwaysUpdateDnnMod3.csproj";
            BuildDnnProject.Build(new System.IO.FileInfo(projectPath));

            return View();
        }

        public IActionResult CreateAlwaysUpdateMod()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateAlwaysUpdateMod(AlwaysUpdateDnnMod alwaysUpdateDnnMod)
        {
            var projectPath = DnnGenerateProject.Generate_From_Url_Json_Always_Updated(alwaysUpdateDnnMod.Name, alwaysUpdateDnnMod.Url, alwaysUpdateDnnMod.User, alwaysUpdateDnnMod.Password);
            BuildDnnProject.Build(new System.IO.FileInfo(projectPath));

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

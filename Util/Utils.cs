﻿using Dnn_Kendo_Generator;
using Dnn_Kendo_Generator.CoeTemplate;
using Dnn_Kendo_Generator.Kendo_Datasource;
using Dnn_Kendo_Generator.Kendo_Grid;
using Dnn_Kendo_Generator.KendoGridColumn;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Xml;

namespace Util
{
    public static class Utils
    {
        private static List<string> get_Json_ColumnNames(string jsonObject)
        {
            var jArray = JArray.Parse(jsonObject); // parse as array  
            return get_Json_ColumnNames(jArray);
        }

        private static List<string> get_Json_ColumnNames(JArray jArray)
        {
            var result = new List<string>();
            if (jArray.Count == 0) return result;

            var root = (JObject)jArray[0];

            foreach (var node in root)
            {
                var colummName = node.Key;
                result.Add(colummName);
            }

            return result;
        }

        private static List<KendoGridColumn> get_Json_Columns(string jsonObject)
        {
            var jArray = JArray.Parse(jsonObject); // parse as array  
            return get_Json_Columns(jArray);
        }

        private static List<KendoGridColumn> get_Json_Columns(JArray jArray)
        {
            var result = new List<KendoGridColumn>();
            if (jArray.Count == 0) return result;

            var root = (JObject)jArray[0];

            foreach (var app in root)
            {
                var colummName = app.Key;
                result.Add(new KendoGridColumn(colummName, colummName.Replace("_", " ")) { Encoded = "false" });
            }

            return result;
        }

        public static List<KendoGridColumn> get_Db_Columns(string connectionString, string database, string table)
        {
            var result = new List<KendoGridColumn>();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var cmd = new SqlCommand($"SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH FROM {database}.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{table}'", connection);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var columnName = reader["COLUMN_NAME"].ToString();
                var dataType = reader["DATA_TYPE"].ToString();
                var characterMaximumLength = reader["CHARACTER_MAXIMUM_LENGTH"].ToString();

                result.Add(new KendoGridColumn(columnName, columnName.Replace("_", " "), dataType, characterMaximumLength == "" ? 0 : int.Parse(characterMaximumLength)));
            }
            connection.Close();
            return result;
        }

        public static IEnumerable<string> get_Db_Tables(string connectionString)
        {
            var result = new List<string>();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var cmd = new SqlCommand("SELECT name FROM sys.Tables", connection);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                result.Add(reader["name"].ToString());
            }
            connection.Close();
            return result;
        }

        public static string get_Db_Name(string connectionString)
        {
            var builder = new SqlConnectionStringBuilder(connectionString);
            return builder.InitialCatalog;
        }

        public static List<KendoComponent> GetComponentsFromJsonString(string _HtmlId, string jsonString)
        {
            //var jsonObject = JObject.Parse(jsonString);
            var jsonObject = JArray.Parse(jsonString);

            return GetComponentsFromJsonObject(_HtmlId, jsonObject);
        }

        public static AlwaysUpdateFromJsonCall GetComponentsFromJson_Always_Updated(string readUrl, string _HtmlId, List<KendoGridColumn> columns)
        {

            var templateMultiple = new CoeTemplateMultiple();

            templateMultiple.Components.Add(new KendoGrid(_HtmlId, new KendoDatasourceFromVar(columns, "dataAjax")));

            return new AlwaysUpdateFromJsonCall(
                readUrl,
                templateMultiple,
                new TimeSpan(0, 0, 60));

            throw new NotImplementedException();
        }

        public static AlwaysUpdateFromJsonCall GetComponentsFromJson_Always_Updated(string readUrl, string _HtmlId, string jsonObjectString)
        {
            CoeTemplateMultiple coeTemplateMultiple;
            try
            {
                coeTemplateMultiple = GetComponentFromJsonObject(_HtmlId, JObject.Parse(jsonObjectString));
            }
            catch (Exception e)
            {
                coeTemplateMultiple = GetComponentFromJsonObject(_HtmlId, JArray.Parse(jsonObjectString));
            }

            return new AlwaysUpdateFromJsonCall(
                readUrl,
                coeTemplateMultiple,
                new TimeSpan(0, 0, 60));
        }

        private static CoeTemplateMultiple GetComponentFromJsonObject(string _HtmlId, JToken jsonToken)
        {
            var result = new CoeTemplateMultiple();

            if (jsonToken.Type == JTokenType.Array)
            {
                var jsonTextArray = jsonToken.ToString();
                result.Components.Add(new KendoGrid(_HtmlId, new KendoDatasourceFromVar(get_Json_Columns(jsonToken as JArray), "dataAjax." + jsonToken.Path)));
            }
            else
            {
                if (jsonToken.Type != JTokenType.Object && jsonToken.Type != JTokenType.Property)
                {
                    result.Components.Add(new CoeTemplateSimple("dataAjax." + jsonToken.Parent.Path));
                }
                else
                {
                    int index = 0;
                    foreach (var item in jsonToken.Children())
                    {
                        result.Components.Add(GetComponentFromJsonObject(_HtmlId + index++, item));
                    }
                }
            }
            return result;
        }

        public static List<KendoComponent> GetComponentsFromJsonString(string readUrl, string _HtmlId, string jsonString)
        {
            try
            {
                return GetComponentsFromJsonObject(readUrl, _HtmlId, JObject.Parse(jsonString));
            }
            catch (Exception)
            {
                return GetComponentsFromJsonObject(readUrl, _HtmlId, JArray.Parse(jsonString));
            }
        }

        public static List<string> GetColumnNamesFromJsonString(string jsonString)
        {
            try
            {
                return GetColumnNamesFromJsonString(JObject.Parse(jsonString));
            }
            catch (Exception)
            {
                return GetColumnNamesFromJsonString(JArray.Parse(jsonString));
            }
        }

        private static List<string> GetColumnNamesFromJsonString(JToken jsonToken)
        {
            if (jsonToken.Type == JTokenType.Array)
            {
                var jsonTextArray = jsonToken.ToString();
                return get_Json_ColumnNames(jsonTextArray);
            }

            if (jsonToken.Type != JTokenType.Object && jsonToken.Type != JTokenType.Property)
            {
                // return new List<KendoComponent> { new KendoHtmlTagFromJsonString(jsonToken.Parent.Path, jsonToken.ToString()) };
            }

            var result = new List<string>();

            foreach (var item in jsonToken.Children())
            {
                if (item.Type == JTokenType.Property)
                {
                    result.Add(((JProperty)item).Name);
                }
            }


            return result;
        }


        private static List<KendoComponent> GetComponentsFromJsonObject(string readUrl, string _HtmlId, JToken jsonToken)
        {
            if (jsonToken.Type == JTokenType.Array)
            {
                var jsonTextArray = jsonToken.ToString();
                return new List<KendoComponent> { new KendoGrid(_HtmlId, new KendoDatasourceReadOnly(readUrl, get_Json_Columns(jsonTextArray))) };
            }

            if (jsonToken.Type != JTokenType.Object && jsonToken.Type != JTokenType.Property)
            {
                return new List<KendoComponent> { new KendoHtmlTagFromJsonString(jsonToken.Parent.Path, jsonToken.ToString()) };
            }

            var result = new List<KendoComponent>();

            foreach (var item in jsonToken.Children())
            {
                result.AddRange(GetComponentsFromJsonObject(_HtmlId, item));
            }


            return result;
        }

        private static List<KendoComponent> GetComponentsFromJsonObject(string _HtmlId, JToken jsonToken)
        {
            /*REVIEW & Update. I don't trust on this function, check https://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_Linq_JTokenType.htm in order to re-do it*/

            if (jsonToken.Type == JTokenType.Array)
            {
                var jsonTextArray = jsonToken.ToString();
                return new List<KendoComponent> { new KendoGrid(_HtmlId, new KendoDatasource(get_Json_Columns(jsonTextArray), jsonTextArray)) };
            }

            if (jsonToken.Type != JTokenType.Object && jsonToken.Type != JTokenType.Property)
            {
                return new List<KendoComponent> { new KendoHtmlTagFromJsonString(jsonToken.Parent.Path, jsonToken.ToString()) };
                //jsonToken.Parent.ToString()

            }

            var result = new List<KendoComponent>();

            //jsonToken is an object but it could contains other objects
            foreach (var item in jsonToken.Children())
            {
                if (item.Type == JTokenType.Array)
                {
                    result.AddRange(GetComponentsFromJsonObject(_HtmlId, item));
                    continue;
                }

                var subItems = item.Children().ToList();
                if (subItems.Count > 0)
                {
                    foreach (var subItem in subItems)
                    {
                        result.AddRange(GetComponentsFromJsonObject(_HtmlId, subItem));
                    }
                }
                else
                {
                    result.AddRange(GetComponentsFromJsonObject(_HtmlId, item));
                }
            }

            return result;
        }

        public static KendoGrid GetComponentsFromHtmlTableString(string htmlTable, string _HtmlId)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(htmlTable);

            return new KendoGrid(_HtmlId, new KendoDatasource(doc));
        }
    }
}

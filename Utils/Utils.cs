﻿using Dnn_Kendo_Generator;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public static class Utils
    {
        public static List<KendoGridColumn> get_Json_Columns(string jsonObject)
        {
            var result = new List<KendoGridColumn>();

            var objects = JArray.Parse(jsonObject); // parse as array  
            JObject root = (JObject)objects[0];

            foreach (KeyValuePair<string, JToken> app in root)
            {
                var COLUMN_NAME = app.Key;
                result.Add(new KendoGridColumn(COLUMN_NAME, COLUMN_NAME.Replace("_", " ")));
            }

            return result;
        }

        public static List<KendoGridColumn> get_Db_Columns(string connectionString, string database, string table)
        {
            var result = new List<KendoGridColumn>();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var cmd = new SqlCommand($"SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH FROM {database}.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{table}'", connection);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var COLUMN_NAME = reader["COLUMN_NAME"].ToString();
                var DATA_TYPE = reader["DATA_TYPE"].ToString();
                var CHARACTER_MAXIMUM_LENGTH = reader["CHARACTER_MAXIMUM_LENGTH"].ToString();

                result.Add(new KendoGridColumn(COLUMN_NAME, COLUMN_NAME.Replace("_", " "), DATA_TYPE, CHARACTER_MAXIMUM_LENGTH == "" ? 0 : int.Parse(CHARACTER_MAXIMUM_LENGTH)));
            }
            connection.Close();
            return result;
        }

        public static string[] get_Db_Tables(string connectionString)
        {
            var result = new List<string>(); ;
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var cmd = new SqlCommand("SELECT name FROM sys.Tables", connection);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                result.Add(reader["name"].ToString());
            }
            connection.Close();
            return result.ToArray();
        }

        public static string get_Db_Name(string connectionString)
        {
            var builder = new SqlConnectionStringBuilder(connectionString);
            return builder.InitialCatalog;
        }


        public static JsonProperty GetJsonProperties(JObject jobject)
        {


            foreach (JProperty property in jobject.Properties())
            {

            }

            throw new NotImplementedException();
        }
    }

    public class JsonProperty
    {
        public string key { get; set; }
        public List<JsonProperty> SubProperties { get; set; }
    }

}

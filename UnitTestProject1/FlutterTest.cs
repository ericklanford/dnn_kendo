﻿using System;
using FlutterGenerator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Net;

namespace UnitTestProject1
{
    [TestClass]
    public class FlutterTest
    {
        [TestMethod]
        public void TestMethod_Create_Single_Empty_Project()
        {
            FluterGenerator.GenerateApp(
                "new_group",
                "Agent Submission Demo",
                new[]
                {
                    new ApiUrl("App_Users","http://sabinepilots.com/DesktopModules/SabineMessagingService/API/Main/GetAppUsers",
                    new NetworkCredential("host","@SabinePilots5148")),
                });
        }


        [TestMethod]
        public void TestMethod_GenerateSchema()
        {
            var result = FluterGenerator.GenerateRegularSchema(
                "AgentSubmissionDemo",
                0,
                "Agent Submission Demo",
                new List<string>
                {
                    "ContactEmail"            ,
                    "VesselName"              ,
                    "IMO"                     ,
                    "OrderType"               ,
                    "VesselCallsign"          ,
                    "Features"                ,
                    "Notes"                   ,
                    "AgencyName"              ,
                    "BillingAddress"          ,
                    "BillingAddress2"         ,
                    "City"                    ,
                    "State"                   ,
                    "Zip"                     ,
                    "AgentID"                 ,
                    "JobDate"                 ,
                    "JobTime"                 ,
                    "ArrivalType"             ,
                    "BoardingArea"            ,
                    "VesselLength"            ,
                    "Beam"                    ,
                    "DWT"                     ,
                    "VesselClass"             ,
                    "DraftFt"                 ,
                    "DraftIn"                 ,
                    "Berth"                   ,
                    "SideTo"                  ,
                    "TugCo"                   ,
                    "LineHandler"             ,
                    "LastPort"                ,
                    "Status"                  ,
                    "SubmittedDate"           ,
                    "SubmittedDnnUser"        ,
                    "submittoday"             ,
                    "submittime"
                    },
                new List<string> { "Pilot", "Agent" }
                );
        }

        [TestMethod]
        public void TestMethod_Generate_Schema_From_Url_Json_Puget()
        {
            const string readUrl = "https://safesound.pspilots.org/DesktopModules/PugetSoundDataService/API/Main/RundownDetailForLogID?logid=612582";
            string cSharpSchemaCode = FluterGenerator.Generate_Schema_From_Url_Json(readUrl, 0, new List<string> { "All" }, "host", "@Puget5023");
            //BuildDnnProject.Build(new System.IO.FileInfo(projectPath));
        }

        [TestMethod]
        public void TestMethod_Generate_Schema_From_Url_Json_Tampa()
        {
            const string readUrl = "http://tampasafe.tampabaypilots.com/tampasafe/DesktopModules/TampaPilotsDataService/API/Main/GetNotes";
            var cSharpSchemaCode = FluterGenerator.Generate_Schema_From_Url_Json(readUrl, 0, new List<string> { "All" }, "host", "@Coesolutions2805");
            Console.WriteLine(cSharpSchemaCode);
            //BuildDnnProject.Build(new System.IO.FileInfo(projectPath));
        }


        [TestMethod]
        public void TestMethod_Generate_Schema_From_Url_Json_localhost()
        {
            const string readUrl = "http://localhost//DesktopModules/AMPDataService/API/App/GetMatrixMatrixVessels";
            string cSharpSchemaCode = FluterGenerator.Generate_Schema_From_Url_Json(readUrl, 0, new List<string> { "All" }, "host", "$Coesolutions805");
            //BuildDnnProject.Build(new System.IO.FileInfo(projectPath));
        }

        [TestMethod]
        public void TestMethod_Generate_Schema_From_Url_Json_LC()
        {
            const string readUrl = "https://lakecharlespilots.com/DesktopModules/LakeCharlesSQLDataService/API/Main/GetRundownExpected";
            string cSharpSchemaCode = FluterGenerator.Generate_Schema_From_Url_Json(readUrl, 0, new List<string> { "All" }, "host", "SAP@$$w0rD");
            //BuildDnnProject.Build(new System.IO.FileInfo(projectPath));
        }

    }
}

﻿using Generator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Sfbp
{
    [TestClass]
    public class UnitTest_Sfbp
    {
        [TestMethod]
        public void TestMethod1()
        {
            const string readUrl = "https://dispatch.sfbp.org/DesktopModules/SFBPDataService/API/Main/GetPIlotCompTimeForYear?year=2021";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Sfbp_GetPIlotCompTimeForYear", readUrl, "kevin.bladsacker@gmail.com", "$Coesolutions805");
        }
        [TestMethod]
        public void TestMethodGetEgentInvoice()
        {
            const string readUrl = "https://dispatch.sfbp.org/DesktopModules/SFBPDataService/API/Main/GetAgentInvoiceLookupsForAgentCode";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Sfbp_GetAgentInvoice", readUrl, "kevin.bladsacker@gmail.com", "$Coesolutions805");
        }
        [TestMethod]
        public void TestMethodGetViewJobPairs()
        {
            const string readUrl = "http://localhost:9880/DesktopModules/SFBPDataService/API/Main/GetViewJobPairs";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Sfbp_ViewJobPairs", readUrl, "host", "@Coesolutions2805");
        }
        [TestMethod]
        public void TestMethodGetViewJobs()
        {
            const string readUrl = "http://localhost:9880/DesktopModules/SFBPDataService/API/Main/GetViewJobs";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Sfbp_ViewJobs", readUrl, "host", "@Coesolutions2805");
        }
    }
}

﻿using Dnn_Kendo_Generator.Kendo_Grid;
using Dnn_Kendo_Generator.Kendo_References;
using Generator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using System.Net;
using Util;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void testColors()
        {
            var rr = Color.Yellow.ToArgb();
        }

        [TestMethod]
        public void TestMethod_Create_Single_Empty_Project()
        {
            var projectPath = DnnGenerateProject.generate_Project("Nobra_Apprentices");
            //BuildDnnProject.Build(new System.IO.FileInfo(projectPath));
        }

        [TestMethod]
        public void TestMethod_Build_Existing_Project()
        {
            var projectPath = @"C:\Dnn_Kendo_Projects\alwaysUpdateDnnMod3\alwaysUpdateDnnMod3.csproj";
            BuildDnnProject.Build(new System.IO.FileInfo(projectPath));
        }

        [TestMethod]
        public void TestMethod_Generate_From_Url_Json_Always_Updated()
        {
            const string readUrl = "https://sqlbak.coesolutions.net/DesktopModules/PilotDispatchDataService/API/Main/GetAppUserDemo";
            var projectPath = DnnGenerateProject.Generate_From_Url_Json_Always_Updated("AppUserDemo", readUrl, "host", "$Coesolutions805");
            //BuildDnnProject.Build(new System.IO.FileInfo(projectPath));
        }

        [TestMethod]
        public void TestMethod_Create_Single_Kendo_Grid_Project()
        {
            DnnGenerateProject.generate_Project("AMP_TransitTimes", new KendoReferences(), new KendoGrid("comp_AMP_TransitTimes"));
        }

        [TestMethod]
        public void TestMethod_Create_Modules_From_Db()
        {
            DnnGenerateProject.generate_Modules_From_Db("Data Source=localhost;Initial Catalog=PPU;Integrated Security=True", "readUrl");
        }

        [TestMethod]
        public void TestMethod_Create_Modules_From_Url()
        {
            DnnGenerateProject.Generate_Modules_From_Url_Json("Test_GoogleApi", "https://maps.googleapis.com/maps/api/geocode/json?&address=Coesolutions&key=AIzaSyDtan4zcGqlvN8CK5oEvwS2CZokyh-hf7s");
        }

        [TestMethod]
        public void TestMethod_Create_Modules_From_Html_Table()
        {
            DnnGenerateProject.Generate_Modules_From_Html_Table("Sav_Scheduled_Jobs", @"<table class=""GridViewStyle"" cellspacing=""0"" cellpadding=""3"" rules=""rows"" id=""ContentPlaceHolder1_grdOrders"" style=""background-color:White;border-color:#E7E7FF;border-width:1px;border-style:None;font-weight:bold;width:835px;border-collapse:collapse;"">
	<caption align=""Left"">
			Scheduled Jobs
	</caption>
	<tbody>
		<tr class=""GridViewHeaderStyle"" align=""left"" style=""color:#F7F7F7;background-color:#4A3C8C;font-weight:bold;"">
			<th scope=""col"">Type</th>
			<th scope=""col"">Start</th>
			<th scope=""col"">Firm</th>
			<th scope=""col"">Conj</th>
			<th scope=""col"">Ship NameAlias</th>
			<th scope=""col"">Berth</th>
			<th scope=""col"">Berthing</th>
			<th scope=""col"">Pilot</th>
			<th scope=""col"">Draft</th>
			<th scope=""col"">Tug</th>
			<th scope=""col"">AirDraft</th>
			<th scope=""col"">Remarks</th>
			<th scope=""col"">Documents</th>
		</tr>
		<tr class=""GridViewRowStyle"" style=""color:#4A3C8C;background-color:#E7E7FF;color:#FF0000;backcolor:#ffffff;"">
			<td>SAIL</td>
			<td>09:00</td>
			<td>F</td>
			<td></td>
			<td onmouseover=""ajax_showTooltip('frmViewShip.aspx?shipID=6212',this);return false"" onmouseout=""ajax_hideTooltip()"">M/Y Calliope</td>
			<td>Market </td>
			<td></td>
			<td>25(2)</td>
			<td></td>
			<td>No Tugs</td>
			<td></td>
			<td>Capt. Adam Connolly  (786)956-0318</td>
			<td/>
		</tr>
	</tbody>
</table>");
        }

        [TestMethod]
        public void TestMethod_Create_Modules_From_Json()
        {
            DnnGenerateProject.Generate_Modules_From_Json_Always_Updated("BarPilot_DailyAvg", "readUrl", @"[
    {
        ""Priority"": 0,
        ""Note1"": ""DA 06/18 27.33 <br> 06/19 PT H 1134 1.235 06/19 PT L 2337 -0.315 <br> 06/19 SW H 0935 1.524 06/19 SW L 2129 -0.169 <br> RS 06/19  1300 16.5 <br>"",
        ""Note2"": null,
        ""LastChangeUser"": ""web"",
        ""LastChangeDateTime"": ""2019-06-19T10:36:32.8286647-05:00""
    }
]");
        }

        [TestMethod]
        public void TestMethod_Create_Modules_From_WebRequest()
        {
            const string readUrl = "https://maps.googleapis.com/maps/api/geocode/json?&address=Coesolutions&key=AIzaSyDtan4zcGqlvN8CK5oEvwS2CZokyh-hf7s";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Test_GoogleApi", readUrl);
        }

        [TestMethod]
        public void TestMethod_Create_Modules_From_WebRequest2()
        {
            const string readUrl = "http://localhost:9000/DesktopModules/AMPDataService/API/Main/GetSmsLogs";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("AMPSmsLogs", readUrl, "host", "$Coesolutions805");
        }

        [TestMethod]
        public void TestMethod_Create_Modules_From_WebRequest3()
        {
            const string readUrl = "http://sabinepilots.com/DesktopModules/SabineMessagingService/API/Main/GetAppUsers";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Sabine_NotifyPlayerIDByUsername", readUrl, "host", "@SabinePilots5148");
        }

        [TestMethod]
        public void TestMethod_Create_Kendo_Components_From_String()
        {
            Utils.GetComponentsFromJsonString("comp_test", "{ 'predictions' : [ {'t':'2018 - 06 - 06 02:07', 'v':'6.405', 'type':'H'} ], 'A':25, 'B':'some text'}");
        }

        [TestMethod]
        public void TestMethod_Create_Kendo_Components_From_Jira()
        {
            const string readUrl = "https://coesolutions.atlassian.net/rest/api/latest/search?jql=project=NOBRA";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Test_Jira", readUrl, "erick@coesolutions.com", "Sayown2008*");
        }

        [TestMethod]
        public void Test1()
        {
            using (var client = new WebClient { Credentials = new NetworkCredential("dispatch@barpilot.com", "dispatch") })
            {
                client.DownloadString("http://bigboard.louisianamaritime.org:8083/get_users");
            }
        }
    }
}

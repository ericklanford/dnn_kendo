﻿using Generator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Local_Calls
{
    [TestClass]
    public class UnitTestLocal
    {
        [TestMethod]
        public void TestMethod1()
        {
            const string readUrl = "http://dnndev.me:9000/DesktopModules/NOBRADataService/API/Main/GetUnofficialBacksheet";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Nobra_GetUnofficialBacksheet", readUrl, "host", "Tzv7ZOEZoN1xAKQn18OX");
        }

        [TestMethod]
        public void TestMethod_Create_Module_Sf_Scheduled_Jobs()
        {
            const string readUrl = "http://dnn.dev.9.5.me:9882/DesktopModules/SFBPDataService/API/Main/GetJobsForDateAndHourRange?start=2020-10-07&hours=72";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Sf_Scheduled_Jobs", readUrl, "host", "@Coesolutions805");
        }
    }
}

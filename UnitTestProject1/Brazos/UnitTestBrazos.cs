﻿using Generator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Brazos
{
    [TestClass]
    public class UnitTestBrazos
    {

        [TestMethod]
        public void TestMethod_Create_Module_GetDnnRoles()
        {
            const string readUrl = "https://dispatch.brazospilots.com/DesktopModules/BPADataService/API/Main/GetDNNRoles";
            DnnGenerateProject.Generate_Modules_From_Url_Json("Brazos_", readUrl, "host", "$Coesolutions805");
        }

        [TestMethod]
        public void TestMethod_Create_Module_GetPilotAlerts()
        {
            const string readUrl = "https://dispatch.brazospilots.com/DesktopModules/BPADataService/API/Main/GetPilotAlerts?PilotNumb=1000";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("Brazos_", readUrl, "host", "$Coesolutions805");
        }


        [TestMethod]
        public void TestMethod_Create_Modules_From_WebRequest3()
        {
            const string readUrl = "https://dispatch.brazospilots.com/DesktopModules/BPADataService/API/Main/GetPilotAlertDetail?PilotId=117";
            DnnGenerateProject.Generate_From_Url_Json_Always_Updated("GetPilotAlertDetail", readUrl, "host", "$Coesolutions805");
        }
    }
}

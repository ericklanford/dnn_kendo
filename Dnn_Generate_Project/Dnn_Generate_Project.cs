﻿using Dnn_Kendo_Generator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace Dnn_Generate_Project
{
    public static class Dnn_Generate_Project
    {
        public static void generate_Project(string project_name)
        {
            var SourcePath = @"..\..\..\Dnn_Kendo_Generator\Base_Dnn_Project\DNN_TEMP_PROJECT_NAME\";
            var DestinationPath = @"C:\Dnn_Kendo_Projects\" + project_name + @"\";

            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
            {
                var current_File_Path = newPath.Replace(SourcePath, DestinationPath);

                File.Copy(newPath, current_File_Path, true);

                string text = File.ReadAllText(current_File_Path);
                text = text.Replace("DNN_TEMP_PROJECT_NAME", project_name);
                File.WriteAllText(current_File_Path, text);
            }

            //replace names
            File.Move(DestinationPath + "DNN_TEMP_PROJECT_NAME.dnn", DestinationPath + project_name + ".dnn");
            File.Move(DestinationPath + "DNN_TEMP_PROJECT_NAME.csproj.user", DestinationPath + project_name + ".csproj.user");
            File.Move(DestinationPath + "DNN_TEMP_PROJECT_NAME.csproj", DestinationPath + project_name + ".csproj");
            File.Move(DestinationPath + "DNN_TEMP_PROJECT_NAMEModuleBase.cs", DestinationPath + project_name + "ModuleBase.cs");
            File.Move(DestinationPath + "DNN_TEMP_PROJECT_NAMEModuleSettingsBase.cs", DestinationPath + project_name + "ModuleSettingsBase.cs");
            File.Move(DestinationPath + "DNN_TEMP_PROJECT_NAME.sln", DestinationPath + project_name + ".sln");

        }

        public static void generate_Project(string project_name, Kendo_References references, Kendo_Grid kendo_component)
        {
            generate_Project(project_name);

            File.AppendAllText(@"C:\Dnn_Kendo_Projects\" + project_name + @"\View.ascx",

                $@"{references.getSource()}
                   {kendo_component.getHtmlPart()}
                    <script>
                        $(function() {{
                            {kendo_component.getSource()}
                        }});
                    </script>"
                );
        }

        public static void generate_Modules_From_Json(string project_name, string jsonObject)
        {
            var columns = Utils.Utils.get_Json_Columns(jsonObject);
            Generate_Projects(project_name, columns);
        }

        public static void generate_Modules_From_Url(string url)
        {
            throw new NotImplementedException();
        }

        private static void Generate_Projects(string project_name, List<KendoGridColumn> columns)
        {
            //simple module
            generate_simple_module(project_name + "_DNN_Simple_module", columns);

            //crud operations
            generate_crud_module(project_name + "_DNN_CRUD_module", new Kendo_Datasource_Crud("readUrl", "updateUrl", "destroyUrl", "createUrl", columns));

            //always online
            generate_always_online_module(project_name + "_DNN_AlwaysOnline_module", columns);
        }

        private static void generate_simple_module(string project_name, List<KendoGridColumn> columns)
        {
            generate_Project(project_name, new Kendo_References(), new Kendo_Grid("readUrl", columns));
        }

        private static void generate_crud_module(string project_name, Kendo_Datasource_Crud kendo_Datasource_Crud)
        {
            generate_Project(project_name, new Kendo_References(), new Kendo_Grid_Crud(kendo_Datasource_Crud));
        }

        private static void generate_always_online_module(string table, List<KendoGridColumn> columns)
        {
            throw new NotImplementedException();
        }

        private static void generate_DataService(string dataService_name)
        {//FIX
            var SourcePath = @"..\..\..\Dnn_Kendo_Generator\Base_Dnn_Project\DemoDataService\";
            var DestinationPath = @"C:\Dnn_Kendo_Projects\" + dataService_name + @"\";

            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
            {
                var current_File_Path = newPath.Replace(SourcePath, DestinationPath);

                File.Copy(newPath, current_File_Path, true);

                string text = File.ReadAllText(current_File_Path);
                text = text.Replace("DemoDataService", dataService_name);
                File.WriteAllText(current_File_Path, text);
            }

            //replace names
            File.Move(DestinationPath + "DemoDataService.dnn", DestinationPath + dataService_name + ".dnn");
            File.Move(DestinationPath + "DemoDataService.csproj.user", DestinationPath + dataService_name + ".csproj.user");
            File.Move(DestinationPath + "DemoDataService.csproj", DestinationPath + dataService_name + ".csproj");
            File.Move(DestinationPath + "DemoDataServiceModuleBase.cs", DestinationPath + dataService_name + "ModuleBase.cs");
            File.Move(DestinationPath + "DemoDataServiceModuleSettingsBase.cs", DestinationPath + dataService_name + "ModuleSettingsBase.cs");
            File.Move(DestinationPath + "DemoDataService.sln", DestinationPath + dataService_name + ".sln");
        }

        #region DB
        public static void generate_Modules_From_Db(string connectionString)
        {
            var db_name = Utils.Utils.get_Db_Name(connectionString);
            var db_tables = Utils.Utils.get_Db_Tables(connectionString);

            generate_DataService(db_name + "_DataService");

            foreach (var table in db_tables)
            {
                var columns = Utils.Utils.get_Db_Columns(connectionString, db_name, table);
                Generate_Projects(table, columns);
            }
        }
        #endregion
    }
}

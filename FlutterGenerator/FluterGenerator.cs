﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Util;

namespace FlutterGenerator
{
    public static class FluterGenerator
    {

        public static string Generate_Schema_From_Url_Json(string readUrl, int screenId, List<string> roles, string user = "", string password = "")
        {
            string response;

            using (var client = user == "" ? new WebClient() : new WebClient { Credentials = new NetworkCredential(user, password) })
            {
                response = client.DownloadString(readUrl);
            }

            var _apiUrl = readUrl.Replace("http://", "").Replace("https://", "");
            _apiUrl = _apiUrl.Substring(_apiUrl.LastIndexOf("/", StringComparison.Ordinal));

            return GenerateRegularSchema(_apiUrl, screenId, _apiUrl, Utils.GetColumnNamesFromJsonString(response), roles);
        }

        public static void GenerateApp(string appName, string appDescription, IEnumerable<ApiUrl> apiUrls)
        {
            var path = CreateProject(appName, appDescription);

            var screens = new List<FlutterWidget>
            {
                //first Screen is going to be home screen
                new FlutterCoeMain(),
                new FlutterCoeHomePage()
            };


            screens.AddRange(ScreensFromUrList(apiUrls));

            WriteScreens(path, screens);
        }

        private static void WriteScreens(string path, List<FlutterWidget> screens)
        {
            screens.ForEach(screen => WriteScreen(path, screen));
        }

        private static void WriteScreen(string path, FlutterWidget screen)
        {
            path += "\\lib\\" + screen.WidgetName + ".dart";

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();


                File.WriteAllText(path, screen.GetSource());

            }
            else if (File.Exists(path))
            {
                throw new FileLoadException();
            }
        }

        private static IEnumerable<FlutterWidget> ScreensFromUrList(IEnumerable<ApiUrl> apiUrls)
        {
            return apiUrls.Select(m => new FlutterWidgetFromApiUrl(m));
        }


        private static string CreateProject(string projectName, string appDescription)
        {
            const string sourcePath = @"C:\Users\xenesel\AndroidStudioProjects\flutter_app\project_name_template\";
            var destinationPath = @"C:\Flutter_Projects\" + projectName + @"\";

            //Now Create all of the directories
            foreach (var dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (var newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                if (newPath.Contains("\\.git")) continue;

                var currentFilePath = newPath.Replace(sourcePath, destinationPath);


                File.Copy(newPath, currentFilePath, true);


                if (currentFilePath.EndsWith(".png") || currentFilePath.EndsWith(".jar"))
                {
                    continue;
                }

                var text = File.ReadAllText(currentFilePath);
                text = text.Replace("project_name_template", projectName);
                File.WriteAllText(currentFilePath, text);
            }


            Directory.Move(destinationPath + "android\\app\\src\\main\\java\\com\\coesolutions\\project_name_template", destinationPath + "android\\app\\src\\main\\java\\com\\coesolutions\\" + projectName);


            //replace names
            File.Move(destinationPath + "project_name_template.iml", destinationPath + projectName + ".iml");
            File.Move(destinationPath + "android\\project_name_template_android.iml", destinationPath + "android\\" + projectName + ".iml");


            return destinationPath;
        }

        public static string GenerateForm(string[] columns)
        {
            return $@"
            SingleChildScrollView(
            child:
                Container(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    children: <Widget>[
                      {string.Join(Environment.NewLine, columns.Select(GenerateFormItem))}
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: RaisedButton(
                              color: Theme.of(context).primaryColorDark,
                              textColor: Theme.of(context).primaryColorLight,
                              child: Text(
                                'Submit',
                                textScaleFactor: 1.5,
                              ),
                              onPressed: () {{
                                setState(() {{
                                  result = _calculate();
                                }});
                              }},
                            ),
                          ),
                          Expanded(
                            child: RaisedButton(
                              color: Theme.of(context).buttonColor,
                              textColor: Theme.of(context).primaryColorDark,
                              child: Text(
                                'Reset',
                                textScaleFactor: 1.5,
                              ),
                              onPressed: () {{
                                setState(() {{
                                  _reset();
                                }});
                              }},
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
            )
            ";
        }

        public static string GenerateFormItem(string column)
        {
            return $@"
            Padding(
                padding:
                    EdgeInsets.only(top: _formDistance, bottom: _formDistance),
                child: TextField(
                  controller: {column.ToLower()}Controller,
                  decoration: InputDecoration(
                      labelText: '{column}',
                      hintText: 'e.g. 124',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                  keyboardType: TextInputType.number,
                ),
              ),
";
        }

        public static string GenerateRegularSchema(string apiUrl, int screenId, string title, List<string> columns, List<string> roles)
        {
            return $@"new ScreenRegular(
                    {screenId},
                    ""{title}"",
                    ""{apiUrl}"",
                    0,
                    new List<ScreenRow>()
                    {{new ScreenRow(
                        new List<ScreenRowItem>()
                            {{
                                {string.Join(",", columns.Select(GenerateScreenRow))}
                            }}
                    )}},
                    null,
                    new List<string> {{ {string.Join(",", roles.Select(m => "\"" + m + "\""))} }},
                    0
                    )";
        }

        private static object GenerateScreenRow(string column)
        {
            return $@"
                        new ScreenRowItem(""{column}"", ""{column}"")
                    ";
        }

        private static object GenerateSchemaItem(string column)
        {
            var titleSpaces = column.Length > 10 ? 2 : 1;
            return $"SchemaColumn('{column.Replace("_", " ")}', '{column}', null, {titleSpaces}, {titleSpaces}),";
        }

        public static string GenerateAppOld(string appName, string title, string[] columns)
        {
            return $@"
                import 'package:flutter/material.dart';

                void main() => runApp(MyApp());

                class MyApp extends StatelessWidget {{
                  @override
                  Widget build(BuildContext context) {{
                    return MaterialApp(
                      debugShowCheckedModeBanner: false,
                      title: '{title}',
                      theme: ThemeData(
                        primarySwatch: Colors.blue,
                      ),
                      home: new {appName}(),
                    );
                  }}
                }}

                class {appName} extends StatefulWidget {{
                  @override
                  State<StatefulWidget> createState() {{
                    return _{appName}State();
                  }}
                }}

                class _{appName}State extends State<{appName}> {{
                    {string.Join(Environment.NewLine, columns.Select(m => $"TextEditingController {m.ToLower()}Controller = TextEditingController();"))}

                String result = """";
                double _formDistance = 15.0;

                  @override
                  Widget build(BuildContext context) {{
                    TextStyle textStyle = Theme.of(context).textTheme.title;
                    return Scaffold(
                        appBar: AppBar(
                          title: Text(""{title}""),
                          backgroundColor: Colors.blueAccent,
                        ),
                        body: 
                        {GenerateForm(columns)}
                        );
                  }}

                  String _calculate() {{                    
                    return """";
                  }}

                  void _reset() {{}}
                }}


";
        }
    }
}

﻿namespace FlutterGenerator
{
    internal class FlutterCoeMain : FlutterWidget
    {
        public FlutterCoeMain() : base("main")
        {
        }

        public override string GetSource()
        {
            return $@"
                import 'package:flutter/material.dart';
                import 'package:{WidgetName}/screens/all.dart';

                //flutter build apk --split-per-abi

                void main() {{
                  runApp(
                    MaterialApp(
                      title: 'Agent Submissions App',
                      home: HomePage(),
                      initialRoute: '/',
                      routes: {{
                        '/login': (context) => LoginPage(),
                        '/list_jobs': (context) => LoginPage(),
                        '/home': (context) => HomePage(),
                      }},
                    ),
                  );
                }}";
        }
    }
}
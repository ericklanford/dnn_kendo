﻿using System.Net;
using System.Security.Policy;

namespace FlutterGenerator
{
    public class ApiUrl
    {
        public ApiUrl(string nameAlias, string apiUrl, NetworkCredential networkCredential)
        {
            NameAlias = nameAlias;
            NetworkCredential = networkCredential;
            Url = new Url(apiUrl);
        }

        public string NameAlias { get; private set; }
        public NetworkCredential NetworkCredential { get; private set; }
        public Url Url { get; private set; }
    }
}
﻿namespace FlutterGenerator
{
    public abstract class FlutterWidget
    {
        public string WidgetName { get; }

        public abstract string GetSource();

        protected FlutterWidget(string widgetFileName)
        {
            WidgetName = widgetFileName;
        }
    }
}
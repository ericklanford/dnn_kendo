﻿namespace FlutterGenerator
{
    internal class FlutterCoeHomePage : FlutterWidget
    {
        public FlutterCoeHomePage() : base("home_page")
        {
        }

        public override string GetSource()
        {
            return $@"
            import 'package:flutter/material.dart';

            class HomePage extends StatefulWidget {{
              @override
              State<StatefulWidget> createState() => _HomePageState();
            }}

            class _HomePageState extends State<HomePage> {{
              int _selectedIndex = 0;
              static List<Widget> _widgetOptions = <Widget>[
                new ListJobs(),
                new MessagesScreen(),
                new ProfileScreen(),
              ];
              static List<Text> titles = <Text>[
                Text(""Home Agent""),
                Text(""Messages""),
                Text(""Profile"")
              ];

              @override
              Widget build(BuildContext formContext) {{
                return Scaffold(
                  appBar: AppBar(
                    //automaticallyImplyLeading: false,
                    title: titles[_selectedIndex],
                    actions: <Widget>[
                      PopupMenuButton<String>(
                        onSelected: select,
                        itemBuilder: (BuildContext context) {{
                          return choices_list_jobs_screen.map((String choice) {{
                            return PopupMenuItem<String>(
                              value: choice,
                              child: Text(choice),
                            );
                          }}).toList();
                        }},
                      ),
                    ],
                  ),
                  body: Center(
                    child: _widgetOptions.elementAt(_selectedIndex),
                  ),
                  bottomNavigationBar: BottomNavigationBar(
                    onTap: onTabTapped, // new
                    currentIndex: _selectedIndex, // new
                    items: [
                      new BottomNavigationBarItem(
                        icon: Icon(Icons.home),
                        title: titles[0],
                      ),
                      new BottomNavigationBarItem(
                        icon: Icon(Icons.mail),
                        title: titles[1],
                      ),
                      new BottomNavigationBarItem(
                          icon: Icon(Icons.person), title: titles[2])
                    ],
                  ),
                  floatingActionButton: FloatingActionButton(
                    onPressed: () {{
                      navigateToAddNew();
                    }},
                    tooltip: ""Add new Todo"",
                    child: new Icon(Icons.add),
                  ),
                  drawer: Drawer(
                    child: FutureBuilder<void>(
                      future: getProfilePageInfo(),
                      builder: (BuildContext context, AsyncSnapshot<void> snapshot) {{
                        switch (snapshot.connectionState) {{
                          case ConnectionState.none:
                          case ConnectionState.active:
                          case ConnectionState.waiting:
                            return Text('Awaiting profile details...');
                          case ConnectionState.done:
                            if (snapshot.hasError) return Text('Error: ${{snapshot.error}}');

                            //profileDetails = jsonDecode(snapshot.data);
                            return Padding(
                              padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                              child: ListView(
                                children: getMenusByUser(),
                              ),
                            );
                        }}
                        return null; // unreachable
                      }},
                    ),
                  ),
                );
              }}

              void onTabTapped(int index) {{
                setState(() {{
                  _selectedIndex = index;
                }});
              }}

              void navigateToAddNew() async {{
                bool result = await Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CreateJob()),
                );
                if (result == true) {{}}
              }}

              void refreshJobs(functionToExecute) async {{
                reloadJobList(functionToExecute);
              }}

              void select(String value) async {{
                switch (value) {{
                  case mnu_AddNewJob_list_jobs_screen:
                    navigateToAddNew();
                    break;
                  case mnu_Refresh_list_jobs_screen:
                    refreshJobs(() {{
                      setState(() {{
                        _widgetOptions[0] = new ListJobs();
                      }});
                    }});
                    break;
                  case mnu_LogOff_list_jobs_screen:
                    logOff().then((response) {{
                      Navigator.pushReplacementNamed(context, '/login');
                    }});
                    break;
                  default:
                    break;
                }}
              }}

              List<Widget> getMenusByUser() {{
                return <Widget>[
                  UserAccountsDrawerHeader(
                    accountName: Text(profileDetails['DisplayName']),
                    accountEmail: Text(profileDetails['LowerEmail']),
                    currentAccountPicture: new CircleAvatar(
                      //backgroundImage: NetworkImage('https://i.pravatar.cc/300'),
                      backgroundImage: NetworkImage(
                          'http://sabinepilots.com/DnnImageHandler.ashx?mode=profilepic&userId=1&h=40&w=40'),
                    ),
                  ),
                  ListTile(
                    title: Text('Current Inbounds'),
                    onTap: () {{
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CurrentInbounds()),
                      );
                    }},
                  ),
                  Divider(),
                  ListTile(
                    title: Text('Current Outbounds'),
                    onTap: () {{
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CurrentOutbounds()),
                      );
                    }},
                  ),
                  Divider(),
                  ListTile(
                    title: Text('Current Harbor Shifts'),
                    onTap: () {{
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CurrentHarborShifts()),
                      );
                    }},
                  ),
                  Divider(),
                  ListTile(
                    title: Text('Projected Inbounds'),
                    onTap: () {{
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProjectedInbounds()),
                      );
                    }},
                  ),
                  Divider(),
                  ListTile(
                    title: Text('Projected Outbounds'),
                    onTap: () {{
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProjectedOutbounds()),
                      );
                    }},
                  ),
                  Divider(),
                  ListTile(
                    title: Text('Projected Shifts'),
                    onTap: () {{
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProjectedShifts()),
                      );
                    }},
                  ),
                  Divider(),
                  ListTile(
                    title: Text('Send Image'),
                    onTap: () {{
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CaptureImage()),
                      );
                    }},
                  ),
                  //Divider(),
                  //Text(playerId),
                ];
              }}
            }} ";
        }
    }
}
﻿namespace FlutterGenerator
{
    internal class FlutterWidgetFromApiUrl : FlutterWidget
    {
        private ApiUrl _apiUrl;

        public FlutterWidgetFromApiUrl(ApiUrl apiUrl) : base(apiUrl.NameAlias.ToLower())
        {
            _apiUrl = apiUrl;
        }

        public override string GetSource()
        {
            return $@"
            import 'dart:convert';
            import 'package:flutter/material.dart';
            import 'package:safe_pilot/controller/grids_controller.dart';
            import 'package:safe_pilot/util/all.dart';
            import 'package:url_launcher/url_launcher.dart';
            import 'dynamic_detail_screen.dart';

            class CurrentInbounds extends StatefulWidget {{
              @override
              State<StatefulWidget> createState() => _CurrentInboundsState();
            }}

            class _CurrentInboundsState extends State<CurrentInbounds> {{
              @override
              Widget build(BuildContext formContext) {{
                var futureModule = FutureBuilder<String>(
                  future: GridsController.getCurrentInbounds(),
                  builder: (BuildContext context, AsyncSnapshot<String> snapshot) {{
                    switch (snapshot.connectionState) {{
                      case ConnectionState.none:
                        return Text('Press button to start.');
                      case ConnectionState.active:
                      case ConnectionState.waiting:
                        return Text('Awaiting result...');
                      case ConnectionState.done:
                        if (snapshot.hasError) return Text('Error: ${{snapshot.error}}');

                        var parsedList = jsonDecode(snapshot.data);

                        if (parsedList.length == 0) {{
                          return Center(
                            child: Text(
                              'Empty List.',
                              textAlign: TextAlign.center,
                            ),
                          );
                        }}

                        for (int i = 0; i < parsedList.length; i++) {{
                          parsedList[i][""StartDate""] =
                              (getDayString(parsedList[i][""EstDateOnBoard""]) +
                                  ' ' +
                                  getHourString(parsedList[i][""OrderDateTime""]));

                          parsedList[i][""OnBoardDateTime""] =
                              getHourString(parsedList[i][""OnBoardDateTime""]);
                        }}

                        var _listViewResult = ListView.builder(
                          itemCount: parsedList.length,
                          itemBuilder: (BuildContext context, int position) {{
                            var colors = getColorsBaseOnVesselProp(parsedList, position);

                            return Card(
                              borderOnForeground: true,
                              //color: this.jobs[position].getStatusCodeColor(),
                              elevation: 4.0,
                              child: Padding(
                                padding: EdgeInsets.only(top: 10, left: 20, right: 10),
                                child: buildCardContent(colors, parsedList, position),
                              ),
                            );
                          }},
                        );

                        return _listViewResult;
                    }}
                    return null; // unreachable
                  }},
                );

                return Scaffold(
                  appBar: AppBar(
                    //automaticallyImplyLeading: false,
                    title: Text(""Projected Inbounds""),
                  ),
                  body: futureModule,
                );
              }}

              void navigateToDetail(title, currentValue) async {{
                bool result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DynamicDetailList(
                      title,
                      currentValue,
                      [
                        ""VesselName"",
                        ""CombinedDraft"",
                        ""StartDate"",
                        ""OnBoardDateTime"",
                        ""CodesDescriptionFrom"",
                        ""CodesDescriptionTo"",
                        ""HiHo"",
                        ""RundownNotes"",
                        ""AgentName"",
                      ],
                      [
                        ""Vessel Name"",
                        ""Draft"",
                        ""Start Date"",
                        ""Boarding Time"",
                        ""Origination"",
                        ""Destination"",
                        ""Hi/Ho"",
                        ""Comments"",
                        ""Agent"",
                      ],
                    ),
                  ),
                );
                if (result == true) {{}}
              }}

              getColorsBaseOnVesselProp(parsedList, int position) {{
                Color cardColor;
                Color vesselNameBg;

                var vesselDetails =
                    parsedList[position][""VesselDetailsInbound""][""intDetail""];

                for (var i = 0; i < vesselDetails.length; i++) {{
                  var currentDataRow = vesselDetails[i];

                  if ((currentDataRow[""RowHeader""] == 'GRT' &&
                          double.parse(currentDataRow[""RowValue""].replaceAll(',', '')) >=
                              85000) ||
                      (currentDataRow[""RowHeader""] == 'LOA' &&
                          double.parse(currentDataRow[""RowValue""]) >= 875) ||
                      (currentDataRow[""RowHeader""] == 'Beam' &&
                          double.parse(currentDataRow[""RowValue""]) >= 125)) {{
                    //Row is Purple if vessel is >84999.99 Gross Tons or >874.99 feet LOA or >124.99 feet beam
                    cardColor = Color(0xffefbbfc); //Colors.purpleAccent;
                    break;
                  }}
                  if ((currentDataRow[""RowHeader""] == 'LOA' &&
                          double.parse(currentDataRow[""RowValue""]) >= 860) ||
                      (currentDataRow[""RowHeader""] == 'Beam' &&
                          double.parse(currentDataRow[""RowValue""]) >= 120)) {{
                    //Row is Yellow if vessel is >859.99 feet LOA or >119.99 feet beam
                    cardColor = Color(0xfff2eeb6); //Colors.yellowAccent;
                  }}
                }}

                //Name cell
                if (parsedList[position][""VesselHoldYesNo""].indexOf('Y') != -1) {{
                  vesselNameBg =
                      Colors.redAccent; //Name cell is Red if Hold YN field is ""Y""
                }} else {{
                  if (parsedList[position][""VesselHoldYesNo""].indexOf('N') != -1 &&
                      parsedList[position][""VesselHoldDescription""].length > 0) {{
                    vesselNameBg = Colors
                        .deepOrangeAccent; //Name cell is Orange if Hold YN field is ""N"" but Desc field is not NULL (NOTE: this is not the ""Restriction"" field)
                  }}
                }}

                return {{""cardColor"": cardColor, ""vesselNameBg"": vesselNameBg}};
              }}

              Widget buildCardContent(colors, parsedList, position) {{
                var columnContent = Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.directions_boat,
                          size: 28,
                          color: Colors.blueAccent,
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            parsedList[position][""VesselName""],
                            style: TextStyle(
                                fontSize: 14, backgroundColor: colors[""vesselNameBg""]),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Text(
                            'Draft:',
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            parsedList[position][""CombinedDraft""],
                            style: TextStyle(fontSize: 14),
                          ),
                        ),
                      ],
                    ),
                    Divider(),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Start Date:',
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Text(
                            parsedList[position][""StartDate""],
                            style: TextStyle(fontSize: 14),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Text(
                            'Boarding:',
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Text(
                            parsedList[position][""OnBoardDateTime""],
                            style: TextStyle(fontSize: 14),
                          ),
                          flex: 1,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        FlatButton(
                          child: Text(
                            'view details',
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                          onPressed: () {{
                            //close(context, parsedList[position]);
                            navigateToDetail(
                                parsedList[position][""VesselName""], parsedList[position]);
                          }},
                        ),
                        FlatButton(
                          child: Text(
                            'View on Marine Traffic',
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                          onPressed: () {{
                            launch(""http://www.marinetraffic.com/en/ais/details/ships/"" +
                                parsedList[position][""VesselImo""]);
                          }},
                        ),
                      ],
                    ),
                  ],
                );

                return colors[""cardColor""] == null
                    ? columnContent
                    : ColorFiltered(
                        colorFilter:
                            ColorFilter.mode(colors[""cardColor""], BlendMode.saturation),
                        //direction: Axis.vertical,
                        child: columnContent,
                      );
              }}
            }}
";
        }
    }
}